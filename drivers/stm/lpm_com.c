/* ---------------------------------------------------------------------------
 * Copyright (C) 2012 STMicroelectronics Limited
 *
 * Author: Pooja Agarwal <pooja.agarwal@st.com>
 * Author: Udit Kumar <udit-dlh.kumar@st.cm>
 * Contributor: Francesco Virlinzi <francesco.virlinzi@st.com>
 * May be copied or modified under the terms of the GNU General Public
 * License.  See linux/COPYING for more information.
 * ----------------------------------------------------------------------------
 */
#include <linux/stm/lpm.h>
#include <linux/platform_device.h>
#include <linux/io.h>
#include <linux/module.h>
#include <linux/interrupt.h>
#include <linux/wait.h>
#include <linux/errno.h>
#include <linux/kthread.h>
#include <linux/semaphore.h>
#include <linux/i2c.h>
#include <linux/delay.h>
#include <linux/stm/platform.h>
#include "lpm_def.h"

#ifdef CONFIG_STM_LPM_DEBUG
#define lpm_debug(fmt, ...) printk(fmt, ##__VA_ARGS__)
#else
#define lpm_debug(fmt, ...)
#endif

/************************************************************************/
/* Purpose : To get driver and firmware version Message format          */
/* Parameter : driver_version and fw_version                            */
/* driver and firmware version will be returned in above parameter      */
/************************************************************************/

int stm_lpm_get_version(struct stm_lpm_version *driver_version,
	struct stm_lpm_version *fw_version)
{
	int err = 0;
	struct lpm_message response = {0};
	struct lpm_internal_send_msg	send_msg;
	/* check paramters */
	if ((driver_version == NULL) || (fw_version == NULL))
		return -EINVAL;

	/*fill data into internal message*/
	LPM_FILL_MSG(send_msg, LPM_MSG_VER, NULL, MSG_ZERO_SIZE);

	/*fill transaction ID and reply type*/
	LPM_FILL_ID_REPLY(send_msg, MSG_ID_AUTO, SBC_REPLY_YES);

	err = lpm_exchange_msg(&send_msg, &response);
	/* check we got no error from SBC and response is as expected */
	if (err >= 0 && (response.command_id & LPM_MSG_REPLY)) {
		/*Copy the received data to user space */
		fw_version->major_comm_protocol = response.msg_data[0] >> 4;
		fw_version->minor_comm_protocol = response.msg_data[0] & 0x0F;
		fw_version->major_soft = response.msg_data[1] >> 4;
		fw_version->minor_soft = response.msg_data[1] & 0x0F;
		fw_version->patch_soft = response.msg_data[2] >> 4;
		fw_version->month = response.msg_data[2] & 0x0F;
		memcpy(&fw_version->day, &response.msg_data[3], 2);

		driver_version->major_comm_protocol = LPM_MAJOR_PROTO_VER;
		driver_version->minor_comm_protocol = LPM_MINOR_PROTO_VER;
		driver_version->major_soft = LPM_MAJOR_SOFT_VER;
		driver_version->minor_soft = LPM_MINOR_SOFT_VER;
		driver_version->patch_soft = LPM_PATCH_SOFT_VER;
		driver_version->month = LPM_BUILD_MONTH;
		driver_version->day = LPM_BUILD_DAY;
		driver_version->year = LPM_BUILD_YEAR;
	}
	return err;
}
EXPORT_SYMBOL(stm_lpm_get_version);

/************************************************************************/
/* Purpose : To set watchdog time out for SBC firmware                  */
/* Parameter : watch dog time out for SBC                               */
/* In case of not setting any value, SBC will use default value         */
/************************************************************************/

int stm_lpm_configure_wdt(u16 time_in_ms)
{
	char msg[2] = {0};
	struct lpm_internal_send_msg	send_msg = {0};
	struct lpm_message response;
	if (!time_in_ms)
		return -EINVAL;
	/*since other machine always in litte endian mode */
	/* then msg[0] will always be LSB */
	msg[1] = (time_in_ms >> 8) & BYTE_MASK;
	msg[0] = time_in_ms  & BYTE_MASK;
	/*Send message for SBC */
	LPM_FILL_MSG(send_msg, LPM_MSG_SET_WDT, msg, 2);
	LPM_FILL_ID_REPLY(send_msg, MSG_ID_AUTO, SBC_REPLY_YES);
	return lpm_exchange_msg(&send_msg, &response);
}
EXPORT_SYMBOL(stm_lpm_configure_wdt);

/************************************************************************/
/* Purpose : To get current state of sbc firmware                       */
/* Parameter : fw_state                                                 */
/* current status of firmware will be returned in above parameter       */
/* if firmware is not loaded then error will be returned                */
/************************************************************************/

int stm_lpm_get_fw_state(enum stm_lpm_sbc_state *fw_state)
{
	int err = 0;
	struct lpm_internal_send_msg send_msg;
	struct lpm_message reply_msg = {0};
	if (fw_state == NULL)
		return -EINVAL;
	LPM_FILL_MSG(send_msg, LPM_MSG_GET_STATUS, NULL, MSG_ZERO_SIZE);
	LPM_FILL_ID_REPLY(send_msg, MSG_ID_AUTO, SBC_REPLY_YES);
	err = lpm_exchange_msg(&send_msg, &reply_msg);
	/* check we got no error from SBC and response is as expected */
	if (err >= 0 && (reply_msg.command_id & LPM_MSG_REPLY))
		*fw_state = reply_msg.msg_data[0];
	return err;
}
EXPORT_SYMBOL(stm_lpm_get_fw_state);


/************************************************************************/
/* Purpose : To set wakeup devices                                      */
/* This API will be called by Linux PM in no irq mode                   */
/* Parameter : Bit map for wakeup deives                                */
/* Host will send either advance command for new protocol (H207) or     */
/* old protocol for (7108) depending upon firmware version              */
/************************************************************************/
int stm_lpm_set_wakeup_device(u16 devices)
{
	if (lpm_fw_proto_version() >= 1) {
		struct stm_lpm_adv_feature feature;
		feature.feature_name = STM_LPM_WU_TRIGGERS;
		feature.feature_parameter[0] = devices & 0xFF;
		feature.feature_parameter[1] = (devices >> 8) & 0xFF;
		return stm_lpm_set_adv_feature(1, &feature);
	} else {
		int err = 0;
		char msg;
		struct lpm_message response;
		struct lpm_internal_send_msg send_msg = {0};
		/* In older protocol wu can be upto eight*/
		msg = devices & 0xFF;
		LPM_FILL_MSG(send_msg, LPM_MSG_SET_WUD, &msg, 1);
		/* Reply from SBC is expected, there is no irq enabled on host*/
		LPM_FILL_ID_REPLY(send_msg, MSG_ID_AUTO, SBC_REPLY_NO_IRQ);
		err = lpm_exchange_msg(&send_msg, &response);
		return err;
	}
}
EXPORT_SYMBOL(stm_lpm_set_wakeup_device);

/************************************************************************/
/* Purpose : To set wakeup time                                         */
/* Parameter : timeout in seconds after which wakeup is required        */
/* wakeup will be done after current time + timeout set                 */
/************************************************************************/
int stm_lpm_set_wakeup_time(u32 timeout)
{
	int err = 0;
	char msg[4] = {0};
	struct lpm_message response;
	struct lpm_internal_send_msg send_msg = {0};
	/*  msg[0] will always be LSB */
	if (is_bigendian())
		timeout = lpm_reverse_integer(timeout);
	/*copy timeout into message */
	memcpy(msg, &timeout, 4);
	LPM_FILL_MSG(send_msg, LPM_MSG_SET_TIMER, msg, 4);
	LPM_FILL_ID_REPLY(send_msg, MSG_ID_AUTO, SBC_REPLY_YES);
	err = lpm_exchange_msg(&send_msg, &response);
	return err;
}
EXPORT_SYMBOL(stm_lpm_set_wakeup_time);

/************************************************************************/
/* Purpose : To set rtc time                                            */
/* Paramter : rtc value                                                 */
/* This API will update rtc for SBC,                                    */
/* SBC could display this RTC when in standby                           */
/************************************************************************/

int stm_lpm_set_rtc(struct rtc_time *new_rtc)
{
	int err = 0;
	char msg[3] = {0};
	struct lpm_message response = {0};
	struct lpm_internal_send_msg send_msg = {0};
	/*copy received values of rtc into message */
	msg[2] = new_rtc->tm_sec;
	msg[1] = new_rtc->tm_min;
	msg[0] = new_rtc->tm_hour;
	LPM_FILL_MSG(send_msg, LPM_MSG_SET_RTC, msg, 3);
	LPM_FILL_ID_REPLY(send_msg, MSG_ID_AUTO, SBC_REPLY_YES);
	err = lpm_exchange_msg(&send_msg, &response);
	return err;
}
EXPORT_SYMBOL(stm_lpm_set_rtc);

/************************************************************************/
/* Purpose : To get wake devcies                                        */
/* This API will be called by Linux PM in no irq mode                   */
/* Parameter: wakeup_device                                             */
/* wakeup device will be returned in above parameter                    */
/* Host will send either advance command for new protocol (H207) or     */
/* old protocol for (7108) depending upon firmware version              */
/************************************************************************/

int stm_lpm_get_wakeup_device(enum stm_lpm_wakeup_devices *wakeup_device)
{
	int err = 0;
	struct lpm_message response = {0};
	struct lpm_internal_send_msg send_msg;
	if (wakeup_device == NULL)
		return -EINVAL;
	if (lpm_fw_proto_version() >= 1) {
		char feature[6];
		err = stm_lpm_get_adv_feature(2, feature);
		if (err >= 0) {
			if (feature[5])
				*wakeup_device = feature[5] << 8;
			else
				*wakeup_device = feature[4];
	}
		return err;
	} else {
		LPM_FILL_MSG(send_msg, LPM_MSG_GET_WUD, NULL, 0);
		LPM_FILL_ID_REPLY(send_msg, MSG_ID_AUTO, SBC_REPLY_NO_IRQ);
		err = lpm_exchange_msg(&send_msg, &response);
		if (err >= 0 && (response.command_id & LPM_MSG_REPLY))
			*wakeup_device = response.msg_data[0];
		return err;
	}
}
EXPORT_SYMBOL(stm_lpm_get_wakeup_device);

/************************************************************************/
/* Purpose : To set front panel                                         */
/* Parameter : fp_setting front panel setting                           */
/* By default Host is assumed to be controlling front panel             */
/************************************************************************/
int stm_lpm_setup_fp(struct stm_lpm_fp_setting *fp_setting)
{
	char msg = 0;
	int err = 0;
	struct lpm_message response;
	struct lpm_internal_send_msg send_msg = {0};
	if (fp_setting == NULL)
		return -EINVAL;

	msg = fp_setting->owner&OWNER_MASK;
	msg |= (fp_setting->am_pm & 1) << 2;
	msg |= (fp_setting->brightness & BRIGHT_MASK) << 4;
	LPM_FILL_MSG(send_msg, LPM_MSG_SET_FP, &msg, 1);
	LPM_FILL_ID_REPLY(send_msg, MSG_ID_AUTO, SBC_REPLY_YES);
	err = lpm_exchange_msg(&send_msg, &response);
	return err;
}
EXPORT_SYMBOL(stm_lpm_setup_fp);

/************************************************************************/
/* Purpose : To inform SBC about PIO Use                                */
/* Parameters      pio_setting                                          */
/* PIO level, PIO number, Pin,   					*/
/* Use (GPIO, POWER CONTROL, EXT IT)					*/
/************************************************************************/

int stm_lpm_setup_pio(struct stm_lpm_pio_setting *pio_setting)
{
	char msg[3] = {0};
	int err = 0;
	struct lpm_message response;
	struct lpm_internal_send_msg send_msg = {0};
	if (pio_setting == NULL || (pio_setting->pio_direction
		 && pio_setting->interrupt_enabled))
		return -EINVAL;

	msg[0] = pio_setting->pio_bank;
	if (pio_setting->pio_use == STM_LPM_PIO_EXT_IT)
		msg[0] = 0xFF;
	msg[1] = pio_setting->pio_level |
	pio_setting->pio_direction |
	pio_setting->interrupt_enabled << 6 |
	pio_setting->pio_pin ;
	msg[2] = pio_setting->pio_use;

	LPM_FILL_MSG(send_msg, LPM_MSG_SET_PIO, msg, 3);
	LPM_FILL_ID_REPLY(send_msg, MSG_ID_AUTO, SBC_REPLY_YES);
	err = lpm_exchange_msg(&send_msg, &response);
	return err;
}
EXPORT_SYMBOL(stm_lpm_setup_pio);

/************************************************************************/
/* Purpose : To inform SBC about Key Scan setting                       */
/* Parameters      key_data                                             */
/* key_data is key number on which SBC will do power on                 */
/************************************************************************/

int stm_lpm_setup_keyscan(u16 key_data)
{
	char msg[2] = {0};
	int err = 0;
	struct lpm_message response;
	struct lpm_internal_send_msg send_msg = {0};
	memcpy(msg, &key_data, 2);

	LPM_FILL_MSG(send_msg, LPM_MSG_SET_KEY_SCAN, msg, 2);
	LPM_FILL_ID_REPLY(send_msg, MSG_ID_AUTO, SBC_REPLY_YES);
	err = lpm_exchange_msg(&send_msg, &response);
	return err;
}
EXPORT_SYMBOL(stm_lpm_setup_keyscan);

/************************************************************************/
/* Purpose : To set advance feature for SBC                             */
/* Parameters      enabled and feature set                              */
/* enable should be set when particular feature needs to be enabled     */
/* feature is feature type and its parameters 				*/
/* feature can be SBC VCORE External without parameter 			*/
/* feature can be SBC Low voltage detect with value of voltage 		*/
/* feature can be SBC clock selection(external, AGC or 32K              */
/* feature can be SBC RTC source 32K_TCXO or 32K_OSC                    */
/* feature can be wakeup triggers with parameter as wakeup devices      */
/************************************************************************/

int stm_lpm_set_adv_feature(u8 enabled, struct stm_lpm_adv_feature *feature)
{
	char msg[4] = {0};
	int err = 0;
	u8 sbc_reply = SBC_REPLY_YES;
	struct lpm_message response;
	struct lpm_internal_send_msg send_msg = {0};
	if (feature == NULL)
		return -EINVAL;
	memcpy((msg+2), &feature->feature_parameter, 2);;
	msg[0] = feature->feature_name;
	msg[1] = enabled;

	LPM_FILL_MSG(send_msg , LPM_MSG_SET_ADV_FEA, msg, 4);
	if (feature->feature_name == STM_LPM_WU_TRIGGERS)
		sbc_reply = SBC_REPLY_NO_IRQ;
	LPM_FILL_ID_REPLY(send_msg, MSG_ID_AUTO, sbc_reply);
	err = lpm_exchange_msg(&send_msg, &response);
	return err;
}
EXPORT_SYMBOL(stm_lpm_set_adv_feature);

/************************************************************************/
/* Purpose : To get advance feature for SBC                             */
/* Parameters      all_feature and pointer to collect value             */
/* when all_feature is set SBC will returned all supported featurers    */
/* return value features[0-3] will be bit map for each feature	        */
/* feature[4-5] will be triggers                                        */
/* if all_feature is set then feature[4-5] will be supported triggers   */
/* if not set then feature[4-5] will be current wakeup triggery         */
/************************************************************************/

int stm_lpm_get_adv_feature(unsigned char all_features, char *features)
{
	char msg[1] = {0};
	int err = 0;
	struct lpm_message response;
	 u8 sbc_reply = SBC_REPLY_YES;
	struct lpm_internal_send_msg send_msg = {0};
	if (features == NULL)
		return -EINVAL;
	if (all_features == 1)
		msg[0] = 1;
	if (all_features == 2)
		sbc_reply = SBC_REPLY_NO_IRQ;
	LPM_FILL_MSG(send_msg, LPM_MSG_GET_ADV_FEA, msg, 1);
	LPM_FILL_ID_REPLY(send_msg, MSG_ID_AUTO, sbc_reply);
	err = lpm_exchange_msg(&send_msg, &response);
	memcpy(features, response.msg_data, 6);
	return err;
}
EXPORT_SYMBOL(stm_lpm_get_adv_feature);


int stm_lpm_setup_fp_pio(struct stm_lpm_pio_setting *pio_setting, u32 long_press_delay, u32 default_reset_delay)
{
	char msg[14] = {0};
	int err = 0;
	struct lpm_message response;
	struct lpm_internal_send_msg send_msg = {0};
	if (pio_setting == NULL || (pio_setting->pio_direction
		 && pio_setting->interrupt_enabled) || pio_setting->pio_use != STM_LPM_PIO_FP_PIO)
		return -EINVAL;

	msg[0] = pio_setting->pio_bank;
	msg[1] = pio_setting->pio_level |
	pio_setting->pio_direction |
	pio_setting->interrupt_enabled << 6 |
	pio_setting->pio_pin ;
	msg[2] = pio_setting->pio_use;
	/*msg[3,4,5] are reserved */ 
	memcpy(&msg[6],&long_press_delay,4);
	memcpy(&msg[10],&default_reset_delay,4);
	LPM_FILL_MSG(send_msg, LPM_MSG_SET_PIO, msg, 14);
	LPM_FILL_ID_REPLY(send_msg, MSG_ID_AUTO, SBC_REPLY_YES);
	err = lpm_exchange_msg(&send_msg, &response);
	return err;
}
EXPORT_SYMBOL(stm_lpm_setup_fp_pio);

/* stm_lpm_setup_power_on_delay - function 
 * purpose : to configure delay at SBC for power on 
 * de_bounce_delay : to make sure 3.3V is glich free
 * dc_stability_delay : delay in ms to make sure
 * 1.1V supply is stable
 */

int stm_lpm_setup_power_on_delay(u16 de_bounce_delay, u16 dc_stability_delay)
{
	char msg[4] = {0};
	int err = 0;
	struct lpm_message response;
	struct lpm_internal_send_msg send_msg = {0};
	memcpy(msg, &de_bounce_delay, 2);
	memcpy((msg+2), &dc_stability_delay, 2);
	LPM_FILL_MSG(send_msg , LPM_MSG_POWER_UP_DELAY, msg, 4);
	LPM_FILL_ID_REPLY(send_msg, MSG_ID_AUTO, SBC_REPLY_YES);
	err = lpm_exchange_msg(&send_msg, &response);
	return err;
}
EXPORT_SYMBOL(stm_lpm_setup_power_on_delay); 

int stm_lpm_set_cec_addr(struct stm_lpm_cec_address *addr)
{
	char msg[4] = {0};
        int err = 0;
        struct lpm_message response;
        struct lpm_internal_send_msg send_msg = {0};
	if(addr == NULL)
		return -EINVAL;
        memcpy(msg, &addr->phy_addr, 2);
        memcpy((msg+2), &addr->logical_addr, 2);
        LPM_FILL_MSG(send_msg , LPM_MSG_CEC_ADDR, msg, 4);
        LPM_FILL_ID_REPLY(send_msg, MSG_ID_AUTO, SBC_REPLY_YES);
        err = lpm_exchange_msg(&send_msg, &response);
        return err;

}
EXPORT_SYMBOL(stm_lpm_set_cec_addr);

int stm_lpm_cec_config(enum stm_lpm_cec_select use, union stm_lpm_cec_params *params)
{
	char msg[14] = {0};
        int err = 0;
        struct lpm_message response;
        struct lpm_internal_send_msg send_msg = {0};
        if(NULL == params)
                return -EINVAL;
	msg[0] = use;
	if(use == STM_LPM_CONFIG_CEC_WU_REASON)
	{
		msg[1]=params->cec_wu_reasons;
	}
	else
	{
		msg[2]=params->cec_msg.size;
		memcpy((msg+3),&params->cec_msg.opcode,msg[2]);
	} 
        LPM_FILL_MSG(send_msg , LPM_MSG_CEC_PARAMS, msg, 14);
        LPM_FILL_ID_REPLY(send_msg, MSG_ID_AUTO, SBC_REPLY_YES);
        err = lpm_exchange_msg(&send_msg, &response);
        return err;

}
EXPORT_SYMBOL(stm_lpm_cec_config);
