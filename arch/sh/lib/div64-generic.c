/*
 * Generic __div64_32 wrapper for __xdiv64_32.
 */

#include <linux/types.h>
#include <asm/div64.h>

extern uint64_t __xdiv64_32(u64 n, u32 d);

uint32_t __div64_32(u64 *xp, u32 y)
{
	uint32_t rem;
	uint64_t q = __xdiv64_32(*xp, y);

	rem = *xp - q * y;
	*xp = q;

	return rem;
}


/**
 * M.Schenk 2011.11.17
 * when linking kernel against libgcc.a i got kernel oops as soon as we call one of the
 * operations __udivdi3, __divdi3 or __moddi3. So the only change to get it work,
 * is to add the libgcc plain c code here .....
 */

/* see http://hdt-project.org/browser/com32/lib/libgcc?rev=93e26ebbe4650a16d23509bb1c0eda161532d660 */

uint64_t __udivmoddi4(uint64_t num, uint64_t den, uint64_t *rem_p)
{
 uint64_t quot = 0, qbit = 1;

  if ( den == 0 ) {
	    //__divide_error();
	    return 0;/* If trap returns... */
	  }

  /* Left-justify denominator and count shift */
  while ( (int64_t)den >= 0 ) {
	    den <<= 1;
    qbit <<= 1;
	  }

	  while ( qbit ) {
	    if ( den <= num ) {
	      num -= den;
	      quot += qbit;
	    }
	    den >>= 1;
	    qbit >>= 1;
	  }

  if ( rem_p )
    *rem_p = num;

	  return quot;
	}


int64_t __divdi3(int64_t num, int64_t den)
{
	int minus = 0;
	int64_t v;

	if ( num < 0 ) {
		num = -num;
		minus = 1;
	}
	if ( den < 0 ) {
		den = -den;
		minus ^= 1;
	}

	v = __udivmoddi4(num, den, NULL);

	if ( minus )
		v = -v;

	return v;
}

uint64_t __udivdi3(uint64_t num, uint64_t den)
{
	return __udivmoddi4(num, den, NULL);
}

int64_t __moddi3(int64_t num, int64_t den)
{
  int minus = 0;
  int64_t v = 0;

  if ( num < 0 ) {
    num = -num;
    minus = 1;
  }
  if ( den < 0 ) {
    den = -den;
    minus ^= 1;
  }

  (void) __udivmoddi4(num, den, &v);
  if ( minus )
    v = -v;

  return v;
}
