/*
 * Copyright (C) 2011  STMicroelectronics
 * Author: Francesco M. Virlinzi  <francesco.virlinzi@st.com>
 *
 * May be copied or modified under the terms of the GNU General Public
 * License V.2 ONLY.
 *
 */

#include <linux/init.h>
#include "early_setup.h"

/*
 * M.Schenk 2012.11.15
 * Early setup to overclock the 7105
 *  Clock setup code from romgen file generated with target-pack version 31
 *  Warning : Remove WHILE_NE32 instructions
 *  cd ./drivers/STAPI_SDK-REL_0.38.1/stapisdk/bin
 *  source setenvalbis.sh STB_8XXX_7105_LINUX
 *  /opt/STM/STLinux-2.4/host/stmc/bin/romgen --show-comment dummy:stb_8xxx:st40,overclk=2
 */
long __initdata early_code[] = {
		/*
		stx7105_clockgena_regs.CKGA_CLKOPSRC_SWITCH_CFG
		*/
		POKE32(0xfe213014, 0x00000000),


		/*
		stx7105_clockgena_regs.CKGA_CLKOPSRC_SWITCH_CFG2
		*/
		POKE32(0xfe213024, 0x00000000),


		/*
		stx7105_clockgena_regs.CKGA_PLL0_ENABLE_FB
		*/
		/*WHILE_NE32(0xfe21301c, 0xffffffff, 0x00000000)*/


		/*
		stx7105_clockgena_regs.CKGA_PLL1_ENABLE_FB
		*/
		/*WHILE_NE32(0xfe213020, 0xffffffff, 0x00000000)*/


/* from here: ELSE(1)  SYSCONF_DEVICEID0 */

		/*
		stx7105_clockgena_regs.CKGA_PLL0_CFG
		*/
		  OR32(0xfe213000, 0x00100000),


		/*
		stx7105_clockgena_regs.CKGA_POWER_CFG
		*/
		  OR32(0xfe213010, 0x00000001),


		/*
		stx7105_clockgena_regs.CKGA_PLL0_CFG
		*/
		  UPDATE32(0xfe213000, 0xfff80000, 0x00001401),


		/*
		stx7105_clockgena_regs.CKGA_POWER_CFG
		*/
		  UPDATE32(0xfe213010, 0xfffffffe, 0x00000000),


		/*
		stx7105_clockgena_regs.CKGA_PLL0_CFG
		*/
//		  WHILE_NE32(0xfe213000, 0x80000000, 0x80000000)


		/*
		stx7105_clockgena_regs.CKGA_PLL0_CFG
		*/
		  UPDATE32(0xfe213000, 0xffefffff, 0x00000000),


		/*
		stx7105_clockgena_regs.CKGA_PLL1_CFG
		*/
		  OR32(0xfe213004, 0x00100000),


		/*
		stx7105_clockgena_regs.CKGA_PLL0HS_DIV0_CFG
		*/
		  POKE32(0xfe213900, 0x00000002),


		/*
		stx7105_clockgena_regs.CKGA_PLL0HS_DIV1_CFG
		*/
		  POKE32(0xfe213904, 0x00000002),


		/*
		stx7105_clockgena_regs.CKGA_PLL0HS_DIV2_CFG
		*/
		  POKE32(0xfe213908, 0x00000002),


		/*
		stx7105_clockgena_regs.CKGA_PLL1_DIV3_CFG
		*/
		  POKE32(0xfe213b0c, 0x0000000f),


		/*
		stx7105_clockgena_regs.CKGA_PLL0LS_DIV4_CFG
		*/
		  POKE32(0xfe213a10, 0x00000000),


		/*
		stx7105_clockgena_regs.CKGA_PLL1_DIV5_CFG
		*/
		  POKE32(0xfe213b14, 0x00000008),


		/*
		stx7105_clockgena_regs.CKGA_PLL1_DIV6_CFG
		*/
		  POKE32(0xfe213b18, 0x00000001),


		/*
		stx7105_clockgena_regs.CKGA_PLL1_DIV7_CFG
		*/
		  POKE32(0xfe213b1c, 0x00000001),


		/*
		stx7105_clockgena_regs.CKGA_PLL0LS_DIV8_CFG
		*/
		  POKE32(0xfe213a20, 0x00000002),


		/*
		stx7105_clockgena_regs.CKGA_PLL0LS_DIV9_CFG
		*/
		  POKE32(0xfe213a24, 0x00000002),


		/*
		stx7105_clockgena_regs.CKGA_PLL0LS_DIV10_CFG
		*/
		  POKE32(0xfe213a28, 0x00000002),


		/*
		stx7105_clockgena_regs.CKGA_PLL0LS_DIV11_CFG
		*/
		  POKE32(0xfe213a2c, 0x00000002),


		/*
		stx7105_clockgena_regs.CKGA_PLL1_DIV12_CFG
		*/
		  POKE32(0xfe213b30, 0x00000002),


		/*
		stx7105_clockgena_regs.CKGA_PLL0LS_DIV13_CFG
		*/
		  POKE32(0xfe213a34, 0x00000017),


		/*
		stx7105_clockgena_regs.CKGA_PLL1_DIV14_CFG
		*/
		  POKE32(0xfe213b38, 0x0000001a),


		/*
		stx7105_clockgena_regs.CKGA_PLL1_DIV15_CFG
		*/
		  POKE32(0xfe213b3c, 0x00000008),


		/*
		stx7105_clockgena_regs.CKGA_PLL0LS_DIV16_CFG
		*/
		  POKE32(0xfe213a40, 0x00000002),


		/*
		stx7105_clockgena_regs.CKGA_PLL0LS_DIV17_CFG
		*/
		  POKE32(0xfe213a44, 0x00000002),


		/*
		stx7105_clockgena_regs.CKGA_PLL1_CFG
		*/
		  OR32(0xfe213004, 0x00100000),


		/*
		stx7105_clockgena_regs.CKGA_POWER_CFG
		*/
		  OR32(0xfe213010, 0x00000002),


		/*
		stx7105_clockgena_regs.CKGA_PLL1_CFG
		*/
		  UPDATE32(0xfe213004, 0xfff80000, 0x00002d03),


		/*
		stx7105_clockgena_regs.CKGA_POWER_CFG
		*/
		  UPDATE32(0xfe213010, 0xfffffffd, 0x00000000),


		/*
		stx7105_clockgena_regs.CKGA_PLL1_CFG
		*/
//		  WHILE_NE32(0xfe213004, 0x80000000, 0x80000000)


		/*
		stx7105_clockgena_regs.CKGA_PLL1_CFG
		*/
		  UPDATE32(0xfe213004, 0xffefffff, 0x00000000),


		/*
		stx7105_clockgena_regs.CKGA_CLKOPSRC_SWITCH_CFG
		*/
		  POKE32(0xfe213014, 0xa655a995),


		/*
		stx7105_clockgena_regs.CKGA_CLKOPSRC_SWITCH_CFG2
		*/
		  POKE32(0xfe213024, 0x00000005),


OP_END_POKES /* END */
};
