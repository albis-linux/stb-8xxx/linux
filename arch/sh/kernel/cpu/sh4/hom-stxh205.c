/*
 * -------------------------------------------------------------------------
 * Copyright (C) 2012  STMicroelectronics
 * Author: Francesco M. Virlinzi  <francesco.virlinzi@st.com>
 *
 * May be copied or modified under the terms of the GNU General Public
 * License V.2 ONLY.  See linux/COPYING for more information.
 *
 * -------------------------------------------------------------------------
 */

#include <linux/init.h>
#include <linux/device.h>
#include <linux/platform_device.h>
#include <linux/suspend.h>
#include <linux/errno.h>
#include <linux/time.h>
#include <linux/delay.h>
#include <linux/irqflags.h>
#include <linux/io.h>

#include <linux/stm/stxh205.h>
#include <linux/stm/sysconf.h>
#include <linux/stm/platform.h>
#include <linux/stm/gpio.h>

#include <asm/irq-ilc.h>

#include "stm_hom.h"
#include <linux/stm/poke_table.h>
#include <linux/stm/synopsys_dwc_ddr32.h>
#include <linux/stm/wakeup_devices.h>




#undef EMY_HOM_TEST

#if defined(EMY_HOM_TEST)
/* @@@ Emery just for tests ...
       for setting wake up conditions */
#include <linux/stm/lpm.h>


//The mark and symbol value must be multiplied by the timeperiod of 10us
//Any NEC key:
struct stm_lpm_ir_keyinfo ir_wakeupkey_NECall =
{
0,     /* ir_id = ??   */
10,    /* Periode      */
12800, /* Time out     */
20,    /* Tolerance    */
{
0,     /* key index    */
17,    /* num patterns */
{
		{0x385, 0x546},		/* Leader code NEC*/
		{0x38, 0x71},			/* Custom code Bit8*/
		{0x38, 0xE1},
		{0x38, 0xE1},
		{0x38, 0x71},
		{0x38, 0x71},
		{0x38, 0x71},
		{0x38, 0x71},
		{0x38, 0xE1},			/* Custom code Bit15*/
		{0x38, 0xE1},			/* Custom code Bit0*/
		{0x38, 0xE1},
		{0x38, 0x71},
		{0x38, 0x71},
		{0x38, 0x71},
		{0x38, 0xE1},
		{0x38, 0xE1},
		{0x38, 0xE1}			/* Custom code Bit7*/
//		{0x0, 0x0}			/* Test to mark the end*/
},
}
};

/* Test funciton to activate IR GPIO WAKEUP*/
static int wkup_ir_gpio(void)
{
    int ret=0;
	struct stm_lpm_pio_setting  configurepio = {0};

	/* Inform this PIO configuration for IR gpio to SBC */
	printk("@@@ IR GPIO configuration\n");
	configurepio.pio_level=STM_LPM_PIO_HIGH;
	configurepio.interrupt_enabled=1;
	configurepio.pio_direction=STM_LPM_PIO_INPUT;
	configurepio.pio_use=STM_LPM_PIO_WAKEUP;
	configurepio.pio_bank=3;
	configurepio.pio_pin=4;
	ret=stm_lpm_setup_pio(&configurepio);
	if (0 != ret) {
		printk("@@@ IR GPIO configuration failed\n");
	}
	return ret;
}



#endif /* EMY_HOM_TEST */








#define DDR3SS_REG		0xfde50000

#define PCLK	30000000
#define BAUDRATE_VAL_M1(bps)    	\
	((((bps * (1 << 14)) + (1 << 13)) / (PCLK / (1 << 6))))

#define SBC_MBX				0xfe4b4000
#define SBC_MBX_WRITE_STATUS(x)		(SBC_MBX + 0x4 + 0x4 * (x))

#ifdef CONFIG_STM_LPM
/*
 * This vercor is used to translate the sbc_wake_reason_bitmap
 * coming from the SBC to the interrupt number Linux uses
 */
int platform_lpm_to_irq[8] = {
	ILC_IRQ(5),	/* IRB */
	ILC_IRQ(31),	/* HDMI_CEC */
	ILC_IRQ(49),	/* Kscan */
	ILC_IRQ(22),	/* Wol */
	ILC_IRQ(6),	/* RTC */
	ILC_IRQ(43),	/* ASC */
	ILC_IRQ(107),	/* HDMI_HotPlug */
	0,		/* NMI (-2)?... */
};
#endif

#ifndef CONFIG_PM_DEEP_STANDBY
#define SBC_GPIO_PORT(_nr)		(0xfe610000 + (_nr) * 0x1000)
#if defined(CONFIG_SH_ST_B2067) || defined(CONFIG_SH_ST_B2064) || defined(CONFIG_SH_ST_B2075) || defined(CONFIG_SH_STB_8083) || defined(CONFIG_SH_STB_8090)
#define LMI_RET_GPIO_PORT		3
#define LMI_RET_GPIO_PIN		2
#else 
#define LMI_RET_GPIO_PORT               3
#define LMI_RET_GPIO_PIN                3
#endif

#define LMI_RETENTION_PIN	stm_gpio(LMI_RET_GPIO_PORT, LMI_RET_GPIO_PIN)

static unsigned long stxh205_hom_table[] __cacheline_aligned = {

/* use sys config  reset bit#2 of sys_config169 address 0xfda50114*/
UPDATE32(0xfda50114, ~(1 << 2), 0),

/*check lmi status sys_status SYSTEM_STATUS154 @0xfda500D8 */
/*check bit#2 set */

WHILE_NE32(0xfda500D8, 0x4, 0x0),

synopsys_ddr32_in_hom(DDR3SS_REG),

/*
 * Enable retention mode gpio
 *
 */
POKE32(SBC_GPIO_PORT(LMI_RET_GPIO_PORT) + STM_GPIO_REG_CLR_POUT,
	 1 << LMI_RET_GPIO_PIN),

/*
 * Send message 'ENTER_PASSIVE' (0x5)
 */
POKE32(SBC_MBX_WRITE_STATUS(0), 0x5),
/* END. */
END_MARKER,

};
#endif /*CONFIG_PM_DEEP_STANDBY*/

static void __iomem *early_console_base;

static struct stm_wakeup_devices stxh205_wkd;

static int stxh205_hom_begin(void)
{
	pr_info("[STM][PM] Analyzing the wakeup devices\n");

	stm_check_wakeup_devices(&stxh205_wkd);

#if defined(EMY_HOM_TEST)
	struct stm_lpm_ir_keyinfo  *ir_wakeupkey;
	printk("@@@ Set 15 secs timer...\n");
	(void)stm_lpm_set_wakeup_time(15);


	ir_wakeupkey = &ir_wakeupkey_NECall;

	printk("@@@ Wake up with IR,\n");
	stm_lpm_setup_ir(1, ir_wakeupkey);
/*
	printk("@@@ Wake up with GPIO,\n");
	(void)wkup_ir_gpio();
*/
#endif

	return 0;
}

static void stxh205_hom_early_console(void)
{

	writel(0x1189 & ~0x80, early_console_base + 0x0c); /* ctrl */
	writel(BAUDRATE_VAL_M1(115200), early_console_base); /* baud */
	writel(20, early_console_base + 0x1c);  /* timeout */
	writel(1, early_console_base + 0x10); /* int */
	writel(0x1189, early_console_base + 0x0c); /* ctrl */

	mdelay(100);

	pr_info("Early console ready\n");
}

static int stxh205_hom_prepare(void)
{
	stm_freeze_board();
	
	/* Notify the wakeup device to the external micro */
	stm_notify_wakeup_devices(&stxh205_wkd);
#ifdef CONFIG_PM_DEEP_STANDBY
	*((int *)(0xfe4b4004)) = 0x5; 
	while(1); 
#endif
	return 0;
}

static int stxh205_hom_complete(void)
{
	/* Enable the INTC2 */
	writel(7, 0xfda30000 + 0x00);	/* INTPRI00 */
	writel(1, 0xfda30000 + 0x60);	/* INTMSKCLR00 */

	stm_restore_board();
	stm_set_wakeup_reason(stm_retrieve_wakeup_reason());
	return 0;
}

static struct stm_mem_hibernation stxh205_hom = {
#ifndef CONFIG_PM_DEEP_STANDBY
	.tbl_addr = (unsigned long)stxh205_hom_table,
	.tbl_size = DIV_ROUND_UP(ARRAY_SIZE(stxh205_hom_table) *
			sizeof(long), L1_CACHE_BYTES),
#endif
	.ops.begin = stxh205_hom_begin,
	.ops.prepare = stxh205_hom_prepare,
	.ops.complete = stxh205_hom_complete,
};

static int __init hom_stxh205_setup(void)
{
	int ret;
#ifndef CONFIG_PM_DEEP_STANDBY
	ret = gpio_request(LMI_RETENTION_PIN, "LMI retention mode");
	if (ret) {
		pr_err("[STM]: [PM]: [HoM]: GPIO for retention mode"
			"not acquired\n");
		return ret;
	};

	gpio_direction_output(LMI_RETENTION_PIN, 1);
#endif 
	ret = stm_hom_register(&stxh205_hom);
#ifndef CONFIG_PM_DEEP_STANDBY
	if (ret) {
		gpio_free(LMI_RETENTION_PIN);
		return ret;
	}
#endif 
	early_console_base = (void *)
		ioremap(stm_asc_configured_devices[stm_asc_console_device]
			->resource[0].start, 0x1000);

	pr_info("[STM]: [PM]: [HoM]: Early console @ %p\n",
		early_console_base);
	stxh205_hom.early_console = stxh205_hom_early_console;

	return ret;
}

module_init(hom_stxh205_setup);
