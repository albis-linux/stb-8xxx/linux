/*
 * Copyright (C) 2011  STMicroelectronics
 * Author: Francesco M. Virlinzi  <francesco.virlinzi@st.com>
 *
 * May be copied or modified under the terms of the GNU General Public
 * License V.2 ONLY.
 *
 */

#define OP_END_POKES                                    0
#define OP_POKE32                                       3
#define OP_OR32                                         6
#define OP_UPDATE32                                     9
#define OP_WHILE_NE32                                   13

#define POKE32(A, VAL)          OP_POKE32, A, VAL
#define OR32(A, VAL)            OP_OR32, A, VAL
#define UPDATE32(A, AND, OR)    OP_UPDATE32, A, AND, OR
#define WHILE_NE32(A, AND, VAL)	OP_WHILE_NE32, A, AND, VAL
