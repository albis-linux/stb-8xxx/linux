/*
 * -------------------------------------------------------------------------
 * Copyright (C) 2011  STMicroelectronics
 * Author: Francesco M. Virlinzi  <francesco.virlinzi@st.com>
 *
 * May be copied or modified under the terms of the GNU General Public
 * License V.2 ONLY.  See linux/COPYING for more information.
 *
 * ------------------------------------------------------------------------- */

#include <linux/init.h>
#include <linux/device.h>
#include <linux/platform_device.h>
#include <linux/suspend.h>
#include <linux/errno.h>
#include <linux/time.h>
#include <linux/delay.h>
#include <linux/irqflags.h>
#include <linux/io.h>
#if 1
#  include <linux/gpio.h>
#  include <linux/gpio_keys.h>
#endif

#include <linux/stm/stxh205.h>
#include <linux/stm/sysconf.h>
#include <linux/stm/clk.h>
#include <linux/stm/wakeup_devices.h>

#include <asm/irq-ilc.h>

#include "stm_suspend.h"
#include <linux/stm/poke_table.h>
#include <linux/stm/synopsys_dwc_ddr32.h>

#define DDR3SS_REG		0xfde50000

#define SYSCFG_BANK_1		0xFDA50000
#define SYSCFG_BANK_1_REG(x)	(SYSCFG_BANK_1 + (x - 100) * 4)
#define SYSTEM_CONFIG_169	SYSCFG_BANK_1_REG(169)
#define SYSTEM_STATUS_154	SYSCFG_BANK_1_REG(154)
#define SYSTEM_STATUS_160	SYSCFG_BANK_1_REG(160)

#define CKGA_PLL_CFG(pll_id, reg_id)	(0x4 * (reg_id) + (pll_id) * 0xc)
#define   CKGA_PLL_CFG_LOCK		(1 << 31)
#define CKGA_POWER_CFG			0x018
#define CKGA_CLKOPSRC_SWITCH_CFG	0x01C
#define CKGA_CLKOPSRC_SWITCH_CFG2	0x020

#define CLK_A1_BASE		0xFDAB8000
#define CLK_A1_ETH_PHY		0xa
#define CLK_A1_GMAC		0xe


#if 1
/* Emery 2014.02.11 Support for S2R
   Some additional settings to (slightly) reduce power consumption
*/

#define SYSCFG_CORAL_BASE_ADDR          (0xfda50000)                  /* BANK 1 Coral: sys config 100-199 */
#define SYSCFG_OPAL_BASE_ADDR           (0xfd541000)                  /* BANK 3 Opal : sys config 400-599 */
#define TVOUT_CPU_BASE_ADDR             (0xfd538000)
#define TVOUT_FDMA_BASE_ADDR            (0xfd000000)

#define SYSCFG_BANK1                    SYSCFG_CORAL_BASE_ADDR        /* BANK1 SYNONYM   */
#define SYSCFG_BANK3                    SYSCFG_OPAL_BASE_ADDR         /* BANK3 SYNONYM   */

#define TVOGLUE_0_MAIN_BASE_ADDRESS     (TVOUT_CPU_BASE_ADDR+0x400)   /* Main glue       */
#define TVOGLUE_0_AUX_BASE_ADDRESS      (TVOUT_FDMA_BASE_ADDR+0xe00)  /* Aux glue        */
#define HD_TVOUT_AUX_GLUE_BASE_ADDRESS  TVOGLUE_0_AUX_BASE_ADDRESS

#define SYSTEM_CONFIG100                (SYSCFG_BANK1 + 0x00000000)   /* ALT FUNC PIO4    */
#define SYSTEM_CONFIG109	            (SYSCFG_BANK1 + 0x00000024)   /* SPDIF            */
#define SYSTEM_CONFIG112                (SYSCFG_BANK1 + 0x00000030)   /* SPDIF            */
#define SYSTEM_CONFIG115                (SYSCFG_BANK1 + 0x0000003C)   /* SPDIF            */ 
#define SYSTEM_CONFIG441                (SYSCFG_BANK3 + 0x000000A4)   /* Audio DAC        */
#define SYSTEM_CONFIG424                (SYSCFG_BANK3 + 0x00000060)   /* Audio FS         */
#define SYSTEM_CONFIG495                (SYSCFG_BANK3 + 0x0000017C)   /* HDMI PLL         */
#define SYSTEM_CONFIG401                (SYSCFG_BANK3 + 0x00000004)   /* EMI PWR DOWN REQ */
#define SYSTEM_CONFIG462                (SYSCFG_BANK3 + 0x000000F8)   /* VIDEO CNTRL      */
#define SYSTEM_CONFIG478                (SYSCFG_BANK3 + 0x00000138)   /* FREQ SYNTH VID1  */
#define HTO_AUX_CTRL_PADS               (HD_TVOUT_AUX_GLUE_BASE_ADDRESS+0x0000) /* SD DAC */
#define HTO_MAIN_CTL_PADS               (TVOGLUE_0_MAIN_BASE_ADDRESS+0x0000)    /* HD DAC */


#define STB_H205_GPIO_POWER_ON_ETH	stm_gpio(3, 3)
#define STB_H205_POWER_ON          	stm_gpio(3, 7)
#define STB_H205_SPDIF          	stm_gpio(4, 1)



/* Enabling some debugging */
#undef IPS_DEBUG
//#define IPS_DEBUG 1

#if defined(IPS_DEBUG)
  // You have to enable this function in kernel drivers/stm/clk.c
  extern void display_clocks(void);
#else
  #define IPS_DEBUG 0
#endif

#endif /* Support for S2R */


static void __iomem *cga0;
static void __iomem *cga1;

static struct clk *a0_ref_clk;
static struct clk *a0_ic_lp_on_clk;

static struct clk *a1_pll0_hs_clk;
static struct clk *a1_ddr_clk;
static struct clk *a1_pll1_ls_clk;
static struct clk *a1_eth_phy_clk;

static struct stm_wakeup_devices stxh205_wkd;

/* *************************
 * STANDBY INSTRUCTION TABLE
 * *************************
 */
static unsigned long stxh205_standby_table[] __cacheline_aligned = {
END_MARKER,

END_MARKER
};

/* *********************
 * MEM INSTRUCTION TABLE
 * *********************
 */
static unsigned long stxh205_mem_table[] __cacheline_aligned = {
synopsys_ddr32_in_self_refresh(DDR3SS_REG),
synopsys_ddr32_phy_standby_enter(DDR3SS_REG),

OR32(CLK_A1_BASE + CKGA_POWER_CFG, 3),
 /* END. */
END_MARKER,

/*
 * Turn-on DDR clock:
 * The DDR subsystem uses the channel coming from A1.HS_0
 * this means there is _no_ ClockGen_D...
 *
 * - turn on the A1.PLLs
 * - wait both the PLLs are locked
 */
UPDATE32(CLK_A1_BASE + CKGA_POWER_CFG, 0, ~0x3),
WHILE_NE32(SYSTEM_STATUS_160, 3, 3),

synopsys_ddr32_phy_standby_exit(DDR3SS_REG),
synopsys_ddr32_out_of_self_refresh(DDR3SS_REG),

END_MARKER
};


#if 1
/* Emery 2014.02.11 Support for S2R
*/
static int stb_h205_phy_reset(void *bus)
{
	/*
	 * IC+ IP101 datasheet specifies 10mS low period and device usable
	 * 2.5mS after rising edge. However experimentally it appear
	 * 10mS is required for reliable functioning.
	 */
	gpio_set_value(STB_H205_GPIO_POWER_ON_ETH, 0);
	mdelay(10);
	gpio_set_value(STB_H205_GPIO_POWER_ON_ETH, 1);
	mdelay(10);

	return 1;
}

static int stb_h205_board_sleep(void)
{
	gpio_set_value(STB_H205_GPIO_POWER_ON_ETH, 0);

//  Unfortunately we cannot do that, produces reset
//  This would power down our peripheral components
//	printk("@@@@@@@@@ Remove POWER_ON\n");
//	gpio_set_value(STB_H205_POWER_ON, 0);
	return 0;
}

static int stb_h205_board_wake(void)
{
//	printk("@@@@@@@@@ Restore POWER_ON\n");
//	gpio_set_value(STB_H205_POWER_ON, 1);
	stb_h205_phy_reset(NULL);
	return 0;
}


/* Store settings of register we modify while going to sleep */
struct reg_data {
	unsigned long  reg_val;
	unsigned long  address;
	char           name[32];
}reg_data_t;

static struct reg_data ips_config[17];
static int    max_register=-1;


/* !!! The macro below needs a local variable ips_config and max_register
       to be defined statically somewhere...
*/

#define BITS_SET(idx,val) (ips_config[idx].reg_val | (val) )
#define BITS_CLEARED(idx,val) (ips_config[idx].reg_val & (~val) )
#define READ_REGISTER(idx,register) ips_config[idx].reg_val=ioread32(register)

#define SAVE_CONFIG(idx,register,reg_name)\
  ips_config[idx].address=register;\
  strncpy(ips_config[idx].name,reg_name,32);\
  max_register=idx;\
  idx++

#define SET_REGISTER(idx,register,val)\
  READ_REGISTER(idx,register); \
  if (IPS_DEBUG) \
    printk("[%2i] %18s : 0x%08lX -> 0x%08lX\n",idx,#register,ips_config[idx].reg_val, BITS_SET(idx,val));\
  iowrite32(BITS_SET(idx,val),register);\
  SAVE_CONFIG(idx,register,#register)

#define CLEAR_REGISTER(idx,register,val)\
  READ_REGISTER(idx,register);\
  if (IPS_DEBUG) \
    printk("[%2i] %18s : 0x%08lX -> 0x%08lX\n",idx,#register,ips_config[idx].reg_val, BITS_CLEARED(idx,val));\
  iowrite32(BITS_CLEARED(idx,val),register);\
  SAVE_CONFIG(idx,register,#register)

#define SAVE_REGISTER(idx,register)\
  READ_REGISTER(idx,register); \
  if (IPS_DEBUG) \
    printk("[%2i] %18s : 0x%08lX -> saved\n",idx,#register,ips_config[idx].reg_val);\
  SAVE_CONFIG(idx,register,#register)


#define RESTORE_REGISTER(idx)\
  if (IPS_DEBUG) \
    printk("[%2i] %18s : 0x%08lX\n",idx,ips_config[idx].name,ips_config[idx].reg_val);\
  iowrite32(ips_config[idx].reg_val,ips_config[idx].address);


static void stxh205_ips_suspend_prepare(void)
{
	int index=0;
/*
 * FMV: All the Audio/Video stuff should be already
 *  manage in the SDK 
 */

	pr_info("[STM][PM] stxh205_ips_suspend_prepare\n");

	/* Power down audio DACs (reset bits 3,4 AUD_ADAC_CTRL) */
	SET_REGISTER(index,SYSTEM_CONFIG441,0x30);

	/* power down audio FS (reset bits 14-10 of AUD_FSYN_CFG) */
	CLEAR_REGISTER(index,SYSTEM_CONFIG424,0x7c00);

	/* disable SD DACs POFF */
	SET_REGISTER(index,HTO_AUX_CTRL_PADS, (1<<9) );

	/* power down VID1 (reset bits 14-10  ) */
	CLEAR_REGISTER(index,SYSTEM_CONFIG478,0x7c00);

	/* disable HD DACs POFF */
	/* This does not work (stay stuck in read register value)
	   according to ST this indicates that it is already deactivated by SDK 
	*/
    // SET_REGISTER(index,HTO_MAIN_CTL_PADS, (1<<11) );


	/* power down HDMI PLL */
	SET_REGISTER(index,SYSTEM_CONFIG495, (1<<31) );

	/* EMI power down request */
	SET_REGISTER(index,SYSTEM_CONFIG401, 0x03 );

	/* VIDEO CONTROL */
	CLEAR_REGISTER(index,SYSTEM_CONFIG462, 0xC0 );



	/* configure as 4.1 as GPIO */
	/* Need to first to reconfigure GPIO4.1 to GPIO (instead of alternate 1)
	   and then first set GPIO to 0
	   See also STMWarapperConfig.c where this is configured
	*/
    
	SAVE_REGISTER(index,SYSTEM_CONFIG109);
	SAVE_REGISTER(index,SYSTEM_CONFIG112);
	SAVE_REGISTER(index,SYSTEM_CONFIG115);
	CLEAR_REGISTER(index,SYSTEM_CONFIG100, 0x30 );
	gpio_request(STB_H205_SPDIF, "SPDIF");
	gpio_direction_output(STB_H205_SPDIF, 0);

#if IPS_DEBUG == 1
    display_clocks();
#endif
}


static void stxh205_ips_suspend_resume(void)
{
	int i=0;

	pr_info("[STM][PM] stxh205_ips_suspend_resume\n");

    for (i=max_register; i>=0 ; i--) {
		RESTORE_REGISTER(i);
	}
}

#endif

static int stxh205_suspend_begin(suspend_state_t state)
{
	pr_info("[STM][PM] Analyzing the wakeup devices\n");

	stm_check_wakeup_devices(&stxh205_wkd);

	return 0;
}

static int stxh205_suspend_core(suspend_state_t state, int suspending)
{
	static long *switch_cfg;
	unsigned long cfg_0_0, cfg_0_1, cfg_1_0, cfg_1_1;
	unsigned long pwr_0, pwr_1;
	int i;

	if (suspending)
		goto on_suspending;

	if (!switch_cfg)
		return 0;

	iowrite32(0, cga0 + CKGA_POWER_CFG);
	/*
	 * A1.PLLs could be arelady enabled in the mem_poketable!
	 * Due to the DDR_Ctrl subsystem,,,
	 * But in case of standby it's re-done here
	 */
	iowrite32(0, cga1 + CKGA_POWER_CFG);

	/* Wait A0.Plls lock */
	for (i = 0; i < 2; ++i)
		while (!(ioread32(cga0 + CKGA_PLL_CFG(i, 1))
			 & CKGA_PLL_CFG_LOCK))
				;

	/* Wait A1.Plls lock */
	while ((ioread32(SYSTEM_STATUS_160) & 3) != 3)
		;

	/* apply the original parents */
	iowrite32(switch_cfg[0], cga0 + CKGA_CLKOPSRC_SWITCH_CFG);
	iowrite32(switch_cfg[1], cga0 + CKGA_CLKOPSRC_SWITCH_CFG2);
	iowrite32(switch_cfg[2], cga1 + CKGA_CLKOPSRC_SWITCH_CFG);
	iowrite32(switch_cfg[3], cga1 + CKGA_CLKOPSRC_SWITCH_CFG2);

	kfree(switch_cfg);

	switch_cfg = NULL;

#if 1
/* Emery 2014.02.11 Support for S2R */
	stb_h205_board_wake();
	stxh205_ips_suspend_resume();
#endif

	pr_debug("[STM][PM] ClockGens A: restored\n");
	return 0;

on_suspending:

	cfg_0_0 = 0xc00fffcc;
	cfg_0_1 = 0x3ffff;
	cfg_1_0 = 0xffffffff;
	cfg_1_1 = 0xfffffff;
	pwr_0 = pwr_1 = 0x3;

	switch_cfg = kmalloc(sizeof(long) * 2 * 2, GFP_ATOMIC);

	if (!switch_cfg)
		goto error;


#if 1
/* Emery 2014.02.11 Support for S2R */

	stxh205_ips_suspend_prepare();
	stb_h205_board_sleep();
#endif

	/* Save the original parents */
	switch_cfg[0] = ioread32(cga0 + CKGA_CLKOPSRC_SWITCH_CFG);
	switch_cfg[1] = ioread32(cga0 + CKGA_CLKOPSRC_SWITCH_CFG2);
	switch_cfg[2] = ioread32(cga1 + CKGA_CLKOPSRC_SWITCH_CFG);
	switch_cfg[3] = ioread32(cga1 + CKGA_CLKOPSRC_SWITCH_CFG2);

/* A0 */
	iowrite32(cfg_0_0, cga0 + CKGA_CLKOPSRC_SWITCH_CFG);
	iowrite32(cfg_0_1, cga0 + CKGA_CLKOPSRC_SWITCH_CFG2);
	iowrite32(pwr_0, cga0 + CKGA_POWER_CFG);

/* A1 */
	/*
	 * WOL is totally based on A1. To be working it needs:
	 * - eth.phy_clk and eth1.mac_clk enabled
	 * - eth.phy_clk @ 25 MHz
	 */
	if (stxh205_wkd.eth_phy_can_wakeup) {
		int pll_id = (a1_pll1_ls_clk == clk_get_parent(a1_eth_phy_clk) ?
			2 : 1);
		cfg_1_0 &= ~(0x3 << (CLK_A1_ETH_PHY * 2));
		cfg_1_0 &= ~(0x3 << (CLK_A1_GMAC * 2));
		cfg_1_0 |= (pll_id << (CLK_A1_ETH_PHY * 2));
		cfg_1_0 |= (pll_id << (CLK_A1_GMAC * 2));
		pwr_1 &= ~pll_id;
	}

/*
 * The DDR subsystem uses an clock-channel coming direclty from A1.
 * This mean we have to be really carefully in the A1 management
 *
 * Here check the system isn't goint to break the DDR Subsystem
 */
	/*
	 * maintain the DDR_clk parent as it is!
	 */
	cfg_1_0 &= ~0x3;
	cfg_1_0 |= (switch_cfg[2] & 0x3);

	iowrite32(cfg_1_0, cga1 + CKGA_CLKOPSRC_SWITCH_CFG);
	iowrite32(cfg_1_1, cga1 + CKGA_CLKOPSRC_SWITCH_CFG2);
	/* maintain the PLL (DDR is using) enabled */
	pwr_1 &= ~((clk_get_parent(a1_ddr_clk) == a1_pll0_hs_clk) ? 1 : 2);

	iowrite32(pwr_1, cga1 + CKGA_POWER_CFG);

	pr_debug("[STM][PM] ClockGens A: saved\n");
	return 0;
error:
	kfree(switch_cfg);

	switch_cfg = NULL;

	return -ENOMEM;
}

static int stxh205_suspend_pre_enter(suspend_state_t state)
{
	return stxh205_suspend_core(state, 1);
}

static int stxh205_suspend_post_enter(suspend_state_t state)
{
	return stxh205_suspend_core(state, 0);
}

static int stxh205_evttoirq(unsigned long evt)
{
	return ((evt == 0xa00) ? ilc2irq(evt) : evt2irq(evt));
}

static struct stm_platform_suspend_t stxh205_suspend __cacheline_aligned = {
	.ops.begin = stxh205_suspend_begin,

	.evt_to_irq = stxh205_evttoirq,
	.pre_enter = stxh205_suspend_pre_enter,
	.post_enter = stxh205_suspend_post_enter,

	.stby_tbl = (unsigned long)stxh205_standby_table,
	.stby_size = DIV_ROUND_UP(ARRAY_SIZE(stxh205_standby_table) *
			sizeof(long), L1_CACHE_BYTES),

	.mem_tbl = (unsigned long)stxh205_mem_table,
	.mem_size = DIV_ROUND_UP(ARRAY_SIZE(stxh205_mem_table)
			* sizeof(long), L1_CACHE_BYTES),

};

static int __init stxh205_suspend_setup(void)
{
	struct sysconf_field *sc[2];
	int i;

	sc[0] = sysconf_claim(SYSCONF(169), 2, 2, "PM");
	/* ClockGen_D.Pll lock status */
	sc[1] = sysconf_claim(SYSCONF(154), 2, 2, "PM");


	for (i = 0; i < ARRAY_SIZE(sc); ++i)
		if (!sc[i])
			goto error;

	a0_ic_lp_on_clk = clk_get(NULL, "CLK_A0_IC_REG_LP_ON");
	a0_ref_clk = clk_get(NULL, "CLK_A0_REF");

	a1_pll0_hs_clk = clk_get(NULL, "CLK_A1_PLL0HS");
	a1_ddr_clk = clk_get(NULL, "CLK_A1_IC_DDRCTRL");
	a1_pll1_ls_clk = clk_get(NULL, "CLK_A1_PLL1LS");
	a1_eth_phy_clk = clk_get(NULL, "CLK_A1_ETH_PHY");

	if (a0_ref_clk == ERR_PTR(-ENOENT) ||
	    a0_ic_lp_on_clk == ERR_PTR(-ENOENT) ||
	    a1_pll0_hs_clk == ERR_PTR(-ENOENT) ||
	    a1_pll0_hs_clk == ERR_PTR(-ENOENT) ||
	    a1_pll1_ls_clk == ERR_PTR(-ENOENT) ||
	    a1_eth_phy_clk == ERR_PTR(-ENOENT))
		goto error;

	cga0 = ioremap_nocache(0xfde98000, 0x1000);
	cga1 = ioremap_nocache(0xfdab8000, 0x1000);

	return stm_suspend_register(&stxh205_suspend);

error:
	pr_err("[STM][PM] Error to acquire the sysconf registers\n");
	for (i = 0; i < ARRAY_SIZE(sc); ++i)
		if (sc[i])
			sysconf_release(sc[i]);

	return -EBUSY;
}

module_init(stxh205_suspend_setup);
