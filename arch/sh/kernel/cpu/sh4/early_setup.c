/*
 * Copyright (C) 2011  STMicroelectronics
 * Author: Francesco M. Virlinzi  <francesco.virlinzi@st.com>
 *
 * File under the terms of the GNU General Public License V.2 ONLY. 
 *
 */
#include <linux/kernel.h>
#include <linux/init.h>
#include <asm/io.h>
#include "early_setup.h"

long __initdata early_code[] __attribute__ ((weak)) = {OP_END_POKES} ;

static __init int stm_exec_code(long *code, int *pc)
{
	switch (code[*pc]) {
	case OP_POKE32:
		iowrite32(code[*pc + 2], code[*pc + 1]);
		break;
	case OP_OR32:
		iowrite32(code[*pc + 2] | ioread32(code[*pc + 1]),
			code[*pc + 1]);
		break;
	case OP_UPDATE32:
		iowrite32(ioread32(code[*pc + 1]) & code[*pc + 2]
			 | code[*pc + 3],
			code[*pc + 1]);
		*pc += 1;
		break;
	case OP_WHILE_NE32:
		while ((ioread32(code[*pc + 1] & code[*pc + 2])
			!= code[*pc + 3]))
				cpu_relax();
		*pc += 1;
		break;
	case OP_END_POKES:
		*pc += 1;
		return 0;
	};
	
	*pc += 3;
	return 1;
}

/**
 * M.Schenk
 * fast clocking controlled through U-BOOT delivered
 * kernel arg fastclk.
 */
#ifdef CONFIG_STM_FAST_CLOCKING
static int fastclk = 0;

static int __init fastclk_setup(char *__unused)
{
	fastclk = 1;
	return 1;
}
__setup("fastclk", fastclk_setup);
#endif


int __init stm_early_setup(void)
{
/**
 * M.Schenk 2013.02.11
 * for STi7105, check Cut and enable OC only for Cut >= 4.x
 */
#if 0
	int pc = 0;

	while (stm_exec_code(early_code, &pc));

	printk("[STM] Early Setup: %d operation done\n", pc);

	return 0;
#else
	int pc = 0;

	if ((cpu_data->type == CPU_STX7105) && (cpu_data->cut_major < 4)) {
		printk("[STM] Early Setup: skip OC on STi7105 < Cut4.x\n");
	}
	else {

#ifdef CONFIG_STM_FAST_CLOCKING
		if (fastclk == 1) {
#endif
		while (stm_exec_code(early_code, &pc));

		printk("[STM] Early Setup: %d operation done\n", pc);

#ifdef CONFIG_STM_FAST_CLOCKING
		}
		else {
			printk("[STM] Early Setup: skip OC due not capable\n");
		}
#endif
	}

	return 0;
#endif
}
