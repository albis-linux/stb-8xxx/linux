/*
 * Copyright (C) 2011  STMicroelectronics
 * Author: Francesco M. Virlinzi  <francesco.virlinzi@st.com>
 *
 * May be copied or modified under the terms of the GNU General Public
 * License V.2 ONLY.
 *
 */

#include <linux/init.h>
#include "early_setup.h"

/*
 * Early setup to overclock the 7108 
 *  Clock setup code from romgen file generated with target-pack version 29
 *  Warning : Remove WHILE_NE32 instructions
 *  romgen --target-connect --show-comment 192.168.1.102:hdk7108stx7108:host,
 *  no_convertor_abort=1,active_cores=host:rt,tapmux_mux=1,dual_cpu=1,
 *  lmi0_size=512,lmi1_size=256,lmi_contig=1,bart_offset=2,overclk=2,
 *  boardrev=2,se=0
 */
long __initdata early_code[] = {

/*
stx7108_clockgena_regs.CLOCKGENA_CLKOPSRC_SWITCH_CFG
*/
POKE32(0xfde98014, 0x00000000),


/*
stx7108_clockgena_regs.CLOCKGENA_CLKOPSRC_SWITCH_CFG2
*/
POKE32(0xfde98024, 0x00000000),


/*
stx7108_clockgena_regs.CLOCKGENA_PLL0_CFG
*/
/* PEEK(0xfde98000) (used target peek value 0x80003203) */


/*
stx7108_clockgena_regs.CLOCKGENA_PLL0_CFG
*/
OR32(0xfde98000, 0x00100000),


/*
stx7108_clockgena_regs.CLOCKGENA_POWER_CFG
*/
/* PEEK(0xfde98010) (used target peek value 0x00000000) */


/*
stx7108_clockgena_regs.CLOCKGENA_POWER_CFG
*/
OR32(0xfde98010, 0x00000001),


/*
stx7108_clockgena_regs.CLOCKGENA_PLL0_CFG
*/
/* PEEK(0xfde98000) (used target peek value 0x00183203) */


/*
stx7108_clockgena_regs.CLOCKGENA_PLL0_CFG
*/
UPDATE32(0xfde98000, 0xfff80000, 0x00004603),


/*
stx7108_clockgena_regs.CLOCKGENA_POWER_CFG
*/
/* PEEK(0xfde98010) (used target peek value 0x00000001) */


/*
stx7108_clockgena_regs.CLOCKGENA_POWER_CFG
*/
UPDATE32(0xfde98010, 0xfffffffe, 0x00000000),


/*
stx7108_clockgena_regs.CLOCKGENA_PLL0_CFG
*/
/* PEEK(0xfde98000) (used target peek value 0x80104603) */


/*
stx7108_clockgena_regs.CLOCKGENA_PLL0_CFG
*/
/*DC WHILE_NE32(0xfde98000, 0x80000000, 0x80000000), DC*/


/*
stx7108_clockgena_regs.CLOCKGENA_PLL0_CFG
*/
/* PEEK(0xfde98000) (used target peek value 0x80104603) */


/*
stx7108_clockgena_regs.CLOCKGENA_PLL0_CFG
*/
UPDATE32(0xfde98000, 0xffefffff, 0x00000000),


/*
stx7108_clockgena_regs.CLOCKGENA_PLL1_CFG
*/
/* PEEK(0xfde98004) (used target peek value 0x80004a05) */


/*
stx7108_clockgena_regs.CLOCKGENA_PLL1_CFG
*/
OR32(0xfde98004, 0x00100000),


/*
stx7108_clockgena_regs.CLOCKGENA_POWER_CFG
*/
/* PEEK(0xfde98010) (used target peek value 0x00000000) */


/*
stx7108_clockgena_regs.CLOCKGENA_POWER_CFG
*/
OR32(0xfde98010, 0x00000002),


/*
stx7108_clockgena_regs.CLOCKGENA_PLL1_CFG
*/
/* PEEK(0xfde98004) (used target peek value 0x00184a05) */


/*
stx7108_clockgena_regs.CLOCKGENA_PLL1_CFG
*/
UPDATE32(0xfde98004, 0xfff80000, 0x00001903),


/*
stx7108_clockgena_regs.CLOCKGENA_POWER_CFG
*/
/* PEEK(0xfde98010) (used target peek value 0x00000002) */


/*
stx7108_clockgena_regs.CLOCKGENA_POWER_CFG
*/
UPDATE32(0xfde98010, 0xfffffffd, 0x00000000),


/*
stx7108_clockgena_regs.CLOCKGENA_PLL1_CFG
*/
/* PEEK(0xfde98004) (used target peek value 0x80101903) */


/*
stx7108_clockgena_regs.CLOCKGENA_PLL1_CFG
*/
/*DC WHILE_NE32(0xfde98004, 0x80000000, 0x80000000), DC*/


/*
stx7108_clockgena_regs.CLOCKGENA_PLL1_CFG
*/
/* PEEK(0xfde98004) (used target peek value 0x80101903) */


/*
stx7108_clockgena_regs.CLOCKGENA_PLL1_CFG
*/
UPDATE32(0xfde98004, 0xffefffff, 0x00000000),
POKE32(0xfde98904, 0x00000006),
POKE32(0xfde98908, 0x00000006),
POKE32(0xfde98b0c, 0x00000001),
POKE32(0xfde98a10, 0x00000000),
POKE32(0xfde98a14, 0x00000000),
POKE32(0xfde98b18, 0x00000000),
POKE32(0xfde98b1c, 0x00000000),
POKE32(0xfde98b20, 0x00000000),
POKE32(0xfde98a24, 0x00000000),
POKE32(0xfde98a28, 0x00000006),
POKE32(0xfde98a2c, 0x00000014),
POKE32(0xfde98a30, 0x00000006),
POKE32(0xfde98a34, 0x0000000d),
POKE32(0xfde98b38, 0x00000013),
POKE32(0xfde98a3c, 0x00000006),
POKE32(0xfde98b40, 0x00000000),
POKE32(0xfde98b44, 0x00000001),


/*
stx7108_clockgena_regs.CLOCKGENA_CLKOPSRC_SWITCH_CFG
*/
POKE32(0xfde98014, 0x6556a594),


/*
stx7108_clockgena_regs.CLOCKGENA_CLKOPSRC_SWITCH_CFG2
*/
POKE32(0xfde98024, 0x0000000a),

OP_END_POKES /* END */
};
