/*
 * -------------------------------------------------------------------------
 * Copyright (C) 2009  STMicroelectronics
 * Copyright (C) 2010  STMicroelectronics
 * Author: Francesco M. Virlinzi  <francesco.virlinzi@st.com>
 *
 * May be copied or modified under the terms of the GNU General Public
 * License V.2 ONLY.  See linux/COPYING for more information.
 *
 * ------------------------------------------------------------------------- */

#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/suspend.h>
#include <linux/errno.h>
#include <linux/time.h>
#include <linux/delay.h>
#include <linux/irqflags.h>
#include <linux/io.h>

#include <linux/stm/clk.h>
#include <linux/stm/stx7111.h>
#include <linux/stm/sysconf.h>
#include <linux/stm/wakeup_devices.h>

#include <asm/irq-ilc.h>

#include "stm_suspend.h"
#include <linux/stm/poke_table.h>

/**
 * M.Schenk 2013.03.27
 * WARNING: do not use msleep, use mdelay in this code
 */

#if 1
/**
 * J.Emery 05.09.2014 
 * We have problems with sata and S2R when USB power is removed.
 * The sate is 2x reset, wich porvokes 10s waiting.
 * We inhibit this waiting in the sata driver when we go to S2R.
 * More details in drivers/ata/libata-eh.c
 */
extern void ata_notify_from_usb(unsigned int);
#endif





/**
 * Enable this to avoid that RS232 console goes into hi-z
 */
/* #define USE_RS232_CONSOLE */


#define CGA							0xfe213000
#define CKGA_PLL(x)					((x) * 4)
#define CKGA_OSC_DIV_CFG(x)			(0x800 + (x) * 4)
#define CKGA_PLL0HS_DIV_CFG(x)		(0x900 + (x) * 4)
#define CKGA_PLL0LS_DIV_CFG(x)		(0xa10 + (x) * 4)
#define CKGA_PLL1_DIV_CFG(x)		(0xb00 + (x) * 4)
#define CKGA_PLL_BYPASS				(1 << 20)
#define CKGA_PLL_LOCK				(1 << 31)
#define CKGA_POWER_CFG				(0x010)
#define CKGA_CLKOPSRC_SWITCH_CFG(x)	(0x014 + ((x) * 0x10))


#define SYSCONF_BASE_ADDR			0xfe001000
#define _SYS_STA(x)					(4 * (x) + 0x8 + SYSCONF_BASE_ADDR)
#define _SYS_CFG(x)					(4 * (x) + 0x100 + SYSCONF_BASE_ADDR)

#define LMI_BASE					0xFE901000
#define LMI_APPD(bank)				(0x1000 * (bank) + 0x14 + lmi)

static void __iomem *cga;
static void __iomem *lmi;

static struct clk *ca_ref_clk;
static struct clk *ca_pll1_clk;
static struct clk *ca_ic_if_100_clk;
static struct clk *ca_eth_phy_clk;
static unsigned long ca_ic_if_100_clk_rate;

/* *************************
 * STANDBY INSTRUCTION TABLE
 * *************************
 */
static unsigned long stx7105_standby_table[] __cacheline_aligned = {
POKE32(CGA + CKGA_OSC_DIV_CFG(17), 31),	/* ic_if_200 */

END_MARKER,
/* reduces OSC_st40 */
POKE32(CGA + CKGA_OSC_DIV_CFG(17), 0),	/* ic_if_200 */

END_MARKER
};

/* *********************
 * MEM INSTRUCTION TABLE
 * *********************
 */
static unsigned long stx7105_mem_table[] __cacheline_aligned = {
/* 1. Enables the DDR self refresh mode */
OR32(_SYS_CFG(38), (1 << 20)),
/* waits until the ack bit is zero */
WHILE_NE32(_SYS_STA(4), 1, 1),

/* Disable the analogue input buffers of the pads */
OR32(_SYS_CFG(12), (1 << 10)),
/* Disable the clock output */
UPDATE32(_SYS_CFG(4), ~(1 << 2), 0),
/* 1.1 Turn-off the ClockGenD */
OR32(_SYS_CFG(11), (1 << 12)),
/* wait clock gen lock */
WHILE_NE32(_SYS_STA(3), 1, 1),

POKE32(CGA + CKGA_OSC_DIV_CFG(17), 31),	/* ic_if_200 */

END_MARKER,

/*
 * On resume the system is too much slow
 * for this reason the main clocks are moved @ 30 MHz
 */
POKE32(CGA + CKGA_OSC_DIV_CFG(17), 0),	/* ic_if_200 @ 30 MHz*/

UPDATE32(_SYS_CFG(12), ~(1 << 10), 0),
/* 1. Turn-on the LMI ClocksGenD */
UPDATE32(_SYS_CFG(11), ~(1 << 12), 0),
/* Wait LMI ClocksGenD lock */
WHILE_NE32(_SYS_STA(3), 1, 0),

/* Enable clock ouput */
OR32(_SYS_CFG(4), (1 << 2)),
/* Reset LMI Pad logic */
OR32(_SYS_CFG(11), (1 << 27)),
/* 2. Disables the DDR self refresh mode */
UPDATE32(_SYS_CFG(38), ~(1 << 20), 0),
/* waits until the ack bit is zero */
WHILE_NE32(_SYS_STA(4), 1, 0),

END_MARKER
};


/* J.Emery : 06.01.2012
 * Port from STLinux 2.3
 */

/* <<< BEGIN OF 2.3 PORT >>> */

#define PIO_PIN_INPUT   4
#define PIO_PIN_OUTPUT  2

#define PIO_BASE(no) (no < 7 ? 0xfd020000 + (no * 0x1000) : 0xfe010000 + ((no - 7) * 0x1000))

static void stx7105_pio_standby_configure_pin(unsigned int port, unsigned int pin, int direction)
{
  iowrite32(1 << pin, PIO_BASE(port) + 0x20 + ((direction & (1 << 0)) ? 0x4 : 0x8));
  iowrite32(1 << pin, PIO_BASE(port) + 0x30 + ((direction & (1 << 1)) ? 0x4 : 0x8));
  iowrite32(1 << pin, PIO_BASE(port) + 0x40 + ((direction & (1 << 2)) ? 0x4 : 0x8));
}

static void stx7105_pio_standby_set_pin(unsigned int port, unsigned int pin, unsigned int value)
{
  iowrite32(1 << pin, PIO_BASE(port) + 0x00 + (value ? 0x4 : 0x8));
}

static unsigned int stx7105_pio_standby_get_pin(unsigned int port, unsigned int pin)
{
  return (ioread32(PIO_BASE(port) + 0x10) & (1 << pin));
}

static unsigned long pio_config[17][3];
static unsigned int pio_pins[5];

static suspend_state_t suspend_state; // Set in ops.begin and necessary in .ops.prepare

static void stx7105_ckgb_suspend_prepare(void);
static void stx7105_ckgb_suspend_resume(void);

/* <<< END OF 2.3 PORT >>> */



static struct stm_wakeup_devices wkd;

static int stx7105_suspend_begin(suspend_state_t state)
{

	suspend_state=state;

	if (state == PM_SUSPEND_STANDBY)
	{
		pr_info("[STM][PM] PM_SUSPEND_STANDBY not supported in stx7105_suspend_begin\n");
		/* To be done */
		return 0;
	}

#if 1
	/* Emery: 09.05.2014 : prevent reset of SATA when USB power loss in S2R */
	// printk("@@@ ata_notify_from_usb ON\n");
	ata_notify_from_usb(1);  /* Emery : see comment above */
#endif

	pr_info("[STM][PM] Analysing the wakeup devices\n");
	stm_check_wakeup_devices(&wkd);

	if (wkd.hdmi_can_wakeup)
	{
		pr_info("[STM][PM] Must keep ic_if_100 @ 100 MHz (HDMI)\n");
		/* Must keep ic_if_100 @ 100 MHz */
		return 0;
	}

//@@@-	ca_ic_if_100_clk_rate = clk_get_rate(ca_ic_if_100_clk);
//@@@-	clk_set_parent(ca_ic_if_100_clk, ca_ref_clk);
	/* 15 MHz is safe to go... */
//@@@-	clk_set_rate(ca_ic_if_100_clk, clk_get_rate(ca_ref_clk)/2);

	return 0;
}

static void stx7105_suspend_wake(void)
{
	if (wkd.hdmi_can_wakeup)
		return;

	/* Restore ic_if_100 to previous rate */
//@@@-	clk_set_parent(ca_ic_if_100_clk, ca_pll1_clk);
//@@@-	clk_set_rate(ca_ic_if_100_clk, ca_ic_if_100_clk_rate);
}


/*
 * FMV:
 * the folloing function replace the operation
 * done in the mem_table to manage the:
 * - idx_pio_12V
 * - idx_pio_5V
 */
static int stx7105_idx_pio(int suspending)
{
	if (suspending) {
		/* PIO1.6 +12V off (HDD/VFD) supply */
		writel((1 << 6) | readl(0xfd021000 + 0x8), 0xfd021000 + 0x8);
		/* PIO3.1 +5V off */
		writel((1 << 1) | readl(0xfd023000 + 0x8), 0xfd023000 + 0x8);
/*@@@+ */	ca_ic_if_100_clk_rate = clk_get_rate(ca_ic_if_100_clk);
/*@@@+ */	clk_set_parent(ca_ic_if_100_clk, ca_ref_clk);
	/* 15 MHz is safe to go... */
/*@@@+ */	clk_set_rate(ca_ic_if_100_clk, clk_get_rate(ca_ref_clk)/2);
		return 0;
	}

	/* PIO1.6 +12V on (HDD/VFD) supply */
	writel((1 << 6) | readl(0xfd021000 + 0x4), 0xfd021000 + 0x4);
	/* PIO3.1 +5V on */
	writel((1 << 1) | readl(0xfd023000 + 0x4), 0xfd023000 + 0x4);
	return 0;
}



static int stx7105_suspend_core(suspend_state_t state, int suspending)
{
	static unsigned char *clka_pll0_div;
	static unsigned char *clka_pll1_div;
	static unsigned long *clka_switch_cfg;
	static unsigned long saved_gplmi_appd;
	int i;

	suspend_state=state;
	if (state == PM_SUSPEND_STANDBY)
	{
		pr_info("[STM][PM] PM_SUSPEND_STANDBY not supported in stx7105_suspend_core\n");
		/* To be done */
		return 0;
	}

	if (suspending)
		goto on_suspending;

	pr_info("[STM][PM] stx7105_suspend_core wake up\n");

	if (!clka_pll0_div) /* there was an error on suspending */
		return 0;

	/* Resuming... */

	/* J.Emery : 06.01.2012
	 * Port from STLinux 2.3
	 */
//	stx7105_ckgb_suspend_resume();
	stx7105_idx_pio(0);

	iowrite32(0, cga + CKGA_POWER_CFG);

	while (!(ioread32(cga + CKGA_PLL(0)) & CKGA_PLL_LOCK))
		;
	while (!(ioread32(cga + CKGA_PLL(1)) & CKGA_PLL_LOCK))
		;

	/* applay the original parents */
	iowrite32(clka_switch_cfg[0], cga + CKGA_CLKOPSRC_SWITCH_CFG(0));
	iowrite32(clka_switch_cfg[1], cga + CKGA_CLKOPSRC_SWITCH_CFG(1));

/*@@@+ */	clk_set_parent(ca_ic_if_100_clk, ca_pll1_clk);
/*@@@+ */	clk_set_rate(ca_ic_if_100_clk, ca_ic_if_100_clk_rate);


	/* restore all the clocks settings */
	for (i = 0; i < 18; ++i) {
		iowrite32(clka_pll0_div[i], ((i < 4) ?
			cga + CKGA_PLL0HS_DIV_CFG(i) :
			cga + CKGA_PLL0LS_DIV_CFG(i)));
		iowrite32(clka_pll1_div[i], cga + CKGA_PLL1_DIV_CFG(i));
	}
	mdelay(10);
	pr_devel("[STM][PM] ClockGen A: restored\n");

	/* restore the APPD */
	iowrite32(saved_gplmi_appd, LMI_APPD(0));

	kfree(clka_pll0_div);
	kfree(clka_pll1_div);
	kfree(clka_switch_cfg);
	clka_switch_cfg = NULL;
	clka_pll0_div = clka_pll1_div = NULL;

	stx7105_suspend_wake();
	return 0;


on_suspending:
	clka_pll0_div = kmalloc(sizeof(char) * 18, GFP_ATOMIC);
	clka_pll1_div = kmalloc(sizeof(char) * 18, GFP_ATOMIC);
	clka_switch_cfg = kmalloc(sizeof(long) * 2, GFP_ATOMIC);

	pr_info("[STM][PM] stx7105_suspend_core supsending\n");

	if (!clka_pll0_div || !clka_pll1_div || !clka_switch_cfg)
		goto error;

	/* save the current APPD setting*/
	saved_gplmi_appd = ioread32(LMI_APPD(0));
	/* disable the APPD */
	iowrite32(0x0, LMI_APPD(0));

	/* save the original settings */
	clka_switch_cfg[0] = ioread32(cga + CKGA_CLKOPSRC_SWITCH_CFG(0));
	clka_switch_cfg[1] = ioread32(cga + CKGA_CLKOPSRC_SWITCH_CFG(1));

	for (i = 0; i < 18; ++i) {
		clka_pll0_div[i] = ioread32(((i < 4) ?
			cga + CKGA_PLL0HS_DIV_CFG(i) :
			cga + CKGA_PLL0LS_DIV_CFG(i)));
		clka_pll1_div[i] = ioread32(cga + CKGA_PLL1_DIV_CFG(i));
		}
	pr_info("[STM][PM] ClockGen A: saved\n");
	mdelay(10);

	/* to avoid the system is to much slow all
	 * the clocks are scaled @ 30 MHz
	 * the final setting is done in the tables
	 */
	for (i = 0; i < 18; ++i)
		iowrite32(0, cga + CKGA_OSC_DIV_CFG(i));

	/* almost all the clocks off */
	iowrite32(0xfffff0ff, cga + CKGA_CLKOPSRC_SWITCH_CFG(0));
	iowrite32(0x3, cga + CKGA_CLKOPSRC_SWITCH_CFG(1));

	if (wkd.hdmi_can_wakeup || wkd.eth_phy_can_wakeup) {
		unsigned long pwr = 0x3; /* Plls Off */
		unsigned long cfg = 0xfffff0ff;

#define CLKA_IC_IF_100_ID	5
#define CLKA_ETH0_PHY_ID	13
		if (wkd.hdmi_can_wakeup) {
			/* needs PLL1 on */
			pr_info("[STM][PM] HDMI can wakeup\n");
			pwr &= ~2;
			cfg &= ~(0x3 << (2 * CLKA_IC_IF_100_ID));
			cfg |= (0x2 << (2 * CLKA_IC_IF_100_ID));
		}
		if (wkd.eth_phy_can_wakeup) {
			unsigned long pll_id;
			pr_info("[STM][PM] PHY can wakeup\n");
			/* identify the eth_phy_clk parent */
			pll_id = (clk_get_parent(ca_eth_phy_clk) ==
				ca_pll1_clk) ? 2 : 1;
			pwr &= ~pll_id;
			cfg &= ~(0x3 << (2 * CLKA_ETH0_PHY_ID));
			cfg |= (pll_id << (2 * CLKA_ETH0_PHY_ID));
		}
		iowrite32(cfg, cga + CKGA_CLKOPSRC_SWITCH_CFG(0));
		iowrite32(pwr, cga + CKGA_POWER_CFG);
	} else {
		iowrite32(3, cga + CKGA_POWER_CFG);
		if (!wkd.lirc_can_wakeup)
		{
			/* J.Emery 22.03.2012
			 * Lirc is not enabled but we still use the IR
			 * We need the 15MHz clock as we set it in stx7105_suspend_begin
			 */
			// clk_set_rate(ca_ic_if_100_clk,
			//	    clk_get_rate(ca_ref_clk)/32);
		}
	}

	/* J.Emery : 06.01.2012
	 * Port from STLinux 2.3
	 */
	stx7105_idx_pio(1);
//	stx7105_ckgb_suspend_prepare();

	return 0;

error:
	kfree(clka_pll1_div);
	kfree(clka_pll0_div);
	kfree(clka_switch_cfg);

	clka_switch_cfg = NULL;
	clka_pll0_div = clka_pll1_div = NULL;

	return -ENOMEM;
}

static int stx7105_suspend_pre_enter(suspend_state_t state)
{
	suspend_state=state;
	return stx7105_suspend_core(state, 1);
}

static int stx7105_suspend_post_enter(suspend_state_t state)
{
	suspend_state=state;
	return stx7105_suspend_core(state, 0);
}


/* J.Emery : 06.01.2012
 * Port from STLinux 2.3
 */

/* <<< BEGIN OF 2.3 PORT >>> */

static void stx7105_pio_suspend_resume(void)
{
	int portno;

	pr_info("[STM][PM] stx7105_pio_suspend_resume\n");

	for (portno = 0; portno < 17; portno++) {
		iowrite32(pio_config[portno][0], PIO_BASE(portno) + 0x20);
		iowrite32(pio_config[portno][1], PIO_BASE(portno) + 0x30);
		iowrite32(pio_config[portno][2], PIO_BASE(portno) + 0x40);
	}

	/* PIO 1.0 restore DVB antenna feeding */
	stx7105_pio_standby_set_pin(1, 0, pio_pins[0]);

	/* PIO 11.0 restore DVB 5V Loop control */
	stx7105_pio_standby_set_pin(11, 0, pio_pins[1]);

	/* PIO1.2 Flash WP */
	stx7105_pio_standby_set_pin(1, 2, 1);
	stx7105_pio_standby_configure_pin(1, 2, PIO_PIN_OUTPUT);
}

typedef struct {
	int port;
	int pinstart;
	int pinstop;
	int direction;
} stx7105_pio_config_t;


static stx7105_pio_config_t stx7105_pio_suspend_config[] = {
	{  0, 0, 7, PIO_PIN_INPUT       },

	{  1, 1, 5, PIO_PIN_INPUT       },
	{  1, 7, 7, PIO_PIN_INPUT       },

	{  2, 4, 4, PIO_PIN_INPUT       },
	{  2, 7, 7, PIO_PIN_INPUT       },

	{  3, 2, 3, PIO_PIN_INPUT       },

#ifndef USE_RS232_CONSOLE
	{  4, 0, 3, PIO_PIN_INPUT       }, /* RS232 !!! check */
#else
	{  4, 2, 3, PIO_PIN_INPUT       }, /* RS232 !!! check */
#endif

	{  5, 1, 7, PIO_PIN_INPUT       },

	{  6, 0, 7, PIO_PIN_INPUT       },

	{  7, 0, 3, PIO_PIN_INPUT       },

	{  9, 7, 7, PIO_PIN_INPUT       }, /* HDMI HPD !!! check */

	{ 10, 0, 7, PIO_PIN_INPUT       },

	{ 11, 1, 7, PIO_PIN_INPUT       },

	{ 12, 0, 7, PIO_PIN_INPUT       },

	{ 13, 0, 7, PIO_PIN_INPUT       },

	{ 14, 0, 7, PIO_PIN_INPUT       },
	/**
	 * Reminder, do not set PCI_PME to input HI-Z ! This blocks the resume
	 * if a video was started before suspend to RAM
	 */
	{ 15, 0, 4, PIO_PIN_INPUT       },
	{ 15, 7, 7, PIO_PIN_INPUT       },

	{ 16, 0, 7, PIO_PIN_INPUT       },
};

static void stx7105_pio_suspend_prepare(void)
{
	int entry;
	int portno;

	pr_info("[STM][PM] stx7105_pio_suspend_prepare\n");

	/* safe current PIO configuration */
	for (portno = 0; portno < 17; portno++) {
		pio_config[portno][0] = ioread32(PIO_BASE(portno) + 0x20);
		pio_config[portno][1] = ioread32(PIO_BASE(portno) + 0x30);
		pio_config[portno][2] = ioread32(PIO_BASE(portno) + 0x40);
	}

	/* configure any unused PIO to input (HI-Z) */
	for (entry = 0; entry < (sizeof(stx7105_pio_suspend_config) / sizeof(stx7105_pio_suspend_config[0])); entry++) {
		stx7105_pio_config_t* cfg = &stx7105_pio_suspend_config[entry];

		for (portno = cfg->pinstart; portno <= cfg->pinstop; portno++)
			stx7105_pio_standby_configure_pin(cfg->port, portno, cfg->direction);
	}

	/* PIO 1.0 DVB antenna feeding */
	if (stx7105_pio_standby_get_pin(1, 0))
		pio_pins[0] = 1;
	else
		pio_pins[0] = 0;

	/* PIO 1.0 disable DVB antenna feeding */
	stx7105_pio_standby_set_pin(1, 0, 0);
	stx7105_pio_standby_configure_pin(1, 0, PIO_PIN_OUTPUT);

	/* PIO 11.0 DVB 5V Loop control */
	if (stx7105_pio_standby_get_pin(11, 0))
		pio_pins[1] = 1;
	else
		pio_pins[1] = 0;

	/* DVB 5V Loop control */
	stx7105_pio_standby_set_pin(11, 0, 0);
	stx7105_pio_standby_configure_pin(11, 0, PIO_PIN_OUTPUT);
}

/* J.Emery : 06.01.2012
 * Port from STLinux 2.3
 *
 * If this was called in stx7105_suspend_prepare (like in STLinux2.3) the box get stuck.
 * So I moved all this to a later phase in the suspending process
 * Now I call this function at the end of stx7105_suspend_core, which is called
 * at pre_enter, i.e. just before really going to sleep
 */

/* <<< BEGIN OF 2.3 PORT >>> */

static unsigned long ckgb_config;

static void stx7105_ckgb_suspend_resume(void)
{
	unsigned long value;

	pr_info("[STM][PM] stx7105_ckgb_suspend_resume\n");

	/* unlock CKGB clocks register */
	iowrite32(0xc0de, 0xfe000010);

	/* restore all CKGB clocks (reset bits [0-12]) */
	iowrite32(ckgb_config, 0xfe0000b0);

	/**
	 * power up FS0 analog and digital parts
	 */

	/* analog power up FS0_NPDA */
	value = ioread32(0xfe000014);
	value |= (1 << 4);
	iowrite32(value, 0xfe000014);

	/* digital up down FS0_NSB bits [3:0] */
	value = ioread32(0xfe000058);
	value |= 0xf;
	iowrite32(value, 0xfe000058);

	/**
	 * M.Schenk 2010.09.02
	 * CKGB FS1 is used for generating the LPC clock.
	 * If we like to wakeup at a given time we need the LPC and
	 * of course the clock.
	 */
#if 0
	/**
	 * power up FS1 analog and digital parts
	 */

	/* analog power up FS1_NPDA */
	value = ioread32(0xfe00005c);
	value |= (1 << 4);
	iowrite32(value, 0xfe00005c);

	/* digital power up FS1_NSB bits [3:0] */
	value = ioread32(0xfe0000a0);
	value |= 0xf;
	iowrite32(value, 0xfe0000a0);
#endif

	/* lock CKGB clocks register */
	iowrite32(0xc1a0, 0xfe000010);
}


static void stx7105_ckgb_suspend_prepare(void)
{
	unsigned long value;

	pr_info("[STM][PM] stx7105_ckgb_suspend_prepare\n");

	/* unlock CKGB clocks register */
	iowrite32(0xc0de, 0xfe000010);

	/* turn off all CKGB clocks (reset bits [0-12]) */
	ckgb_config = ioread32(0xfe0000b0);
	iowrite32(ckgb_config & ~0x1fff, 0xfe0000b0);

	/**
	 * power down FS0 analog and digital parts
	 */

	/* analog power down FS0_NPDA */
	value = ioread32(0xfe000014);
	value &= ~(1 << 4);
	iowrite32(value, 0xfe000014);

	/* digital power down FS0_NSB bits [3:0] */
	value = ioread32(0xfe000058);
	value &= ~0xf;
	iowrite32(value, 0xfe000058);

	/**
	 * M.Schenk 2010.09.02
	 * CKGB FS1 is used for generating the LPC clock.
	 * If we like to wakeup at a given time we need the LPC and
	 * of course the clock.
	 */
#if 0
	/**
	 * power down FS1 analog and digital parts
	 */

	/* analog power down FS1_NPDA */
	value = ioread32(0xfe00005c);
	value &= ~(1 << 4);
	iowrite32(value, 0xfe00005c);

	/* digital power down FS1_NSB bits [3:0] */
	value = ioread32(0xfe0000a0);
	value &= ~0xf;
	iowrite32(value, 0xfe0000a0);
#endif

	/* lock CKGB clocks register */
	iowrite32(0xc1a0, 0xfe000010);

}


/* <<< END OF 2.3 PORT >>> */



static unsigned long ips_config[17];

static void stx7105_ips_suspend_resume(void)
{
	int cnt;
	unsigned long value;

	pr_info("[STM][PM] stx7105_ips_suspend_resume\n");

	/* power on audio DACs (set bits 3,5 & 6 of AUD_ADAC_CTRL) */
	iowrite32(ips_config[0] , 0xfe210100);

	/* power on audio FS (reset bits 14-10 of AUD_FSYN_CFG) */
	iowrite32(ips_config[1] , 0xfe210000);

	/* enable SD & HD DACs (set bits 5 - 0 of SYS_CONFIG3) */
	iowrite32(ips_config[2], 0xfe00110c);

	/* power on HDMI */
	iowrite32(ips_config[3], 0xfe001108);

	/* power up HDMI PLL */
	if (ips_config[4]) {
		value = ioread32(0xfe00110c);

		value |= (1 << 12);
		iowrite32(value, 0xfe00110c);

		/* waits until the ack bit is one */
		for (cnt = 0; cnt < 10; cnt++) {
			/* query SYS_STATUS9 HDMI_PLL_LOCK bit */
			value = ioread32(0xfe00102c);

			if ((value & 0x1) == 0x1)
				break;

			mdelay(10);
		}
	}
}

static void stx7105_ips_suspend_prepare(void)
{
	int cnt;
	unsigned long value;
/*
 * FMV: All the Audio/Video stuff should be already
 *  manage in the SDK
 */

	pr_info("[STM][PM] stx7105_ips_suspend_prepare\n");

	/* power down audio DACs (reset bits 3,5 & 6 of AUD_ADAC_CTRL) */
	ips_config[0] = ioread32(0xfe210100);
	iowrite32(ips_config[0] & ~0x68, 0xfe210100);

	/* power down audio FS (reset bits 14-10 of AUD_FSYN_CFG) */
	ips_config[1] = ioread32(0xfe210000);
	iowrite32(ips_config[1] & ~0x7c00, 0xfe210000);

	/* disable SD & HD DACs (set bits 5 - 0 of SYS_CONFIG3) */
	ips_config[2] = ioread32(0xfe00110c);
	iowrite32(ips_config[2] | 0x3f, 0xfe00110c);

	/* power down HDMI */
	ips_config[3] = ioread32(0xfe001108);
	iowrite32(ips_config[3] | (1 << 26), 0xfe001108);

	/* power down HDMI PLL */
	ips_config[4] = 0;

	value = ioread32(0xfe00110c);

	/* check if HDMI PLL need to power off */
	if (value & (1 << 12)) {
		ips_config[4] = 1;

		value &= ~(1 << 12);
		iowrite32(value, 0xfe00110c);

		/* waits until the ack bit is zero */
		for (cnt = 0; cnt < 10; cnt++) {
			/* query SYS_STATUS9 HDMI_PLL_LOCK bit */
			value = ioread32(0xfe00102c);

			if ((value & 0x1) == 0)
				break;

			mdelay(10);
		}
	}
}

// static unsigned long a344;

static int stx7105_suspend_prepare(void)
{
	if (suspend_state == PM_SUSPEND_STANDBY)
	{
		pr_info("[STM][PM] PM_SUSPEND_STANDBY not supported in stx7105_suspend_prepare\n");
		/* To be done */
		return 0;
	}

	pr_info("[STM][PM] stx7105_suspend_prepare\n");

	stx7105_pio_suspend_prepare();

	stx7105_ckgb_suspend_prepare();

	stx7105_ips_suspend_prepare();

	/**
	 * Code from STAPI OS21 suspend which is needed for reliable wakeup.....
	 * Sadly to say that i don't own register documentation so this are just
	 * magic IRQ peeks and pokes.
	 * ./STAPI_SDK-REL_0.24.0/stapisdk/apilib/src/stpower/src/cache/cache_execution.c
	 */
	{
		#define INTC2BaseAddress  0xFE001000
		#define ILC_BASE_ADDRESS  0xFD000000

		unsigned long tmp;

/* Emery 12 Jul 2001
 * This code would enable what is disabled (through a patch from ST) in st40_ilc_stx7200.c,
 * where we disable the ILC Wakeup_out signal. So disable it here too ...
 * Don't ask me too much, I just write down what Francesco said to me ...
 */

#if 0
		/*INTC2*/
		a344 = ioread32(INTC2BaseAddress + 0x344);
		/* *(int*)(INTC2BaseAddress + 0x304) = 0xAC007706;   */ /*Priority*/
		// *(int*)(INTC2BaseAddress + 0x364) = 0x00001000 ;  /*Mask*/

		iowrite32(0x00001000 , INTC2BaseAddress + 0x364);

		/*ILC3*/
		//*(int*)(ILC_BASE_ADDRESS + 0xA24) = 0x4;    /*ILC_EXT_MODE4*/
		iowrite32(0x4 , ILC_BASE_ADDRESS + 0xA24);

		// *(int*)(ILC_BASE_ADDRESS + 0xA20) = 0x8000; /*ILC_EXT_PRIORITY4*/
		iowrite32(0x8000 , ILC_BASE_ADDRESS + 0xA20);

		// *(int*)(ILC_BASE_ADDRESS + 0x608) |= (1<<4);/*ILC_WAKEUP_ENABLE2 */

		tmp = ioread32(ILC_BASE_ADDRESS + 0x608);
		iowrite32(tmp | (1<<4) , ILC_BASE_ADDRESS + 0x608);
#endif
	}

	return 0;
}

static void stx7105_suspend_finish(void)
{
	if (suspend_state == PM_SUSPEND_STANDBY)
	{
		pr_info("[STM][PM] PM_SUSPEND_STANDBY not supported in stx7105_suspend_finish\n");
		/* To be done */
		return;
	}

#if 1
	/* Emery: 09.05.2014 : prevent reset of SATA when USB power loss in S2R */
	// printk("@@@ ata_notify_from_usb OFF\n");
	ata_notify_from_usb(0);
#endif

	pr_info("[STM][PM] stx7105_suspend_finish\n");


/* If enable this code we have still problems to come back from suspend to RAM */
#if 0
	/**
	 * Code from STAPI OS21 suspend which is needed for reliable wakeup.....
	 * Sadly to say that i don't own register documentation so this are just
	 * magic IRQ peeks and pokes.
	 * ./STAPI_SDK-REL_0.24.0/stapisdk/apilib/src/stpower/src/cache/cache_execution.c
	 */

	//    *(int*)(INTC2BaseAddress + 0x344) = a344;
	iowrite32(a344 , INTC2BaseAddress + 0x344);

	//     *(int*)(ILC_BASE_ADDRESS + 0x608) = 0;/*ILC_WAKEUP_ENABLE2 */
	iowrite32(0 , ILC_BASE_ADDRESS + 0x608);
#endif

	/* suspend to RAM */

	stx7105_pio_suspend_resume();

	stx7105_ckgb_suspend_resume();

	stx7105_ips_suspend_resume();


	return;
}

/* <<< BEGIN OF 2.3 PORT >>> */




static int stx7105_evt_to_irq(unsigned long evt)
{
	return ((evt < 0x400) ? ilc2irq(evt) : evt2irq(evt));
}

static struct stm_platform_suspend_t stx7105_suspend __cacheline_aligned = {

	.ops.begin = stx7105_suspend_begin,
	.ops.prepare = stx7105_suspend_prepare,
	.ops.finish = stx7105_suspend_finish,

	.evt_to_irq = stx7105_evt_to_irq,
	.pre_enter = stx7105_suspend_pre_enter,
	.post_enter = stx7105_suspend_post_enter,

	.stby_tbl = (unsigned long)stx7105_standby_table,
	.stby_size = DIV_ROUND_UP(ARRAY_SIZE(stx7105_standby_table) *
			sizeof(long), L1_CACHE_BYTES),

	.mem_tbl = (unsigned long)stx7105_mem_table,
	.mem_size = DIV_ROUND_UP(ARRAY_SIZE(stx7105_mem_table) * sizeof(long),
			L1_CACHE_BYTES),
};

static int __init stx7105_suspend_setup(void)
{

	struct sysconf_field *sc[7];
	int i;

	sc[0] = sysconf_claim(SYS_STA, 4, 0, 0, "pm");
	sc[1] = sysconf_claim(SYS_STA, 3, 0, 0, "pm");
	sc[2] = sysconf_claim(SYS_CFG, 4, 2, 2, "pm");
	sc[3] = sysconf_claim(SYS_CFG, 11, 12, 12, "pm");
	sc[4] = sysconf_claim(SYS_CFG, 11, 27, 27, "pm");
	sc[5] = sysconf_claim(SYS_CFG, 12, 10, 10, "pm");
	sc[6] = sysconf_claim(SYS_CFG, 38, 20, 20, "pm");

	for (i = ARRAY_SIZE(sc)-1; i; --i)
		if (!sc[i])
			goto error;

	cga = ioremap(CGA, 0x1000);
	lmi = ioremap(LMI_BASE, 0x1000);

	ca_ref_clk = clk_get(NULL, "CLKA_REF");
	ca_pll1_clk = clk_get(NULL, "CLKA_PLL1");
	ca_ic_if_100_clk = clk_get(NULL, "CLKA_IC_IF_100");
	ca_eth_phy_clk = clk_get(NULL, "CLKA_ETH0_PHY");

	if (!ca_ref_clk || !ca_pll1_clk ||
	    !ca_ic_if_100_clk || !ca_eth_phy_clk)
		goto error;

	return stm_suspend_register(&stx7105_suspend);

error:
	for (i = ARRAY_SIZE(sc)-1; i; --i)
		if (sc[i])
			sysconf_release(sc[i]);
	return 0;
}

module_init(stx7105_suspend_setup);
