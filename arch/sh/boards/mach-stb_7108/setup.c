/*
 * arch/sh/boards/mach-stb_7108/setup.c
 *
 * (C) Copyright 2008-2013 ALBIS Technologies.
 * Author: Michael Schenk <michael.schenk@albistechnologies.com>
 *
 * May be copied or modified under the terms of the GNU General Public
 * License.  See linux/COPYING for more information.
 */

/**
 * M.Schenk remark, reference is arch/sh/boards/mach-hdk7108/setup.c
 */

#include <linux/init.h>
#include <linux/platform_device.h>
#include <linux/pci.h>
#include <linux/delay.h>
#include <linux/io.h>
#include <linux/phy.h>
#include <linux/gpio.h>
#include <linux/leds.h>
#include <linux/tm1668.h>
#include <linux/mtd/mtd.h>
#include <linux/mtd/physmap.h>
#include <linux/mtd/partitions.h>
#include <linux/mtd/nand.h>
#include <linux/spi/spi.h>
#include <linux/spi/spi_gpio.h>
#include <linux/spi/flash.h>
#include <linux/stm/nand.h>
#include <linux/stm/emi.h>
#include <linux/stm/pci-glue.h>
#include <linux/stm/platform.h>
#include <linux/stm/stx7108.h>
#include <linux/stm/sysconf.h>
#include <asm/irq-ilc.h>

#define STB_7108_PIO_POWER_ON stm_gpio(5, 0)
#define STB_7108_PIO_POWER_ON_ETHERNET stm_gpio(15, 4)
#define STB_7108_GPIO_FLASH_WP stm_gpio(5, 5)
#define STB_7108_GPIO_SPI_HOLD stm_gpio(2, 2)
#define STB_7108_GPIO_SPI_WRITE_PRO stm_gpio(2, 3)

#define STB_7108_GPIO_LPM_I2C	stm_gpio(26, 7)


static void __init stb_7108_setup(char **cmdline_p)
{
#if defined(CONFIG_SH_STB_9000)
	printk(KERN_INFO "Albis Technologies Ldt STB-9000 board initialisation\n");
#elif defined(CONFIG_SH_STB_9090)
	printk(KERN_INFO "Albis Technologies Ldt STB-9090 board initialisation\n");
#elif defined(CONFIG_SH_STB_9300)
	printk(KERN_INFO "Albis Technologies Ldt STB-9300 board initialisation\n");
#else
	printk(KERN_INFO "Albis Technologies Ldt STB-9xxx board initialisation\n");
#endif

	stx7108_early_device_init();

	stx7108_configure_asc(3, &(struct stx7108_asc_config) {
			.routing.asc3.txd = stx7108_asc3_txd_pio24_4,
			.routing.asc3.rxd = stx7108_asc3_rxd_pio24_5,
			.hw_flow_control = 0,
			.is_console = 1, });
	stx7108_configure_asc(1, &(struct stx7108_asc_config) {
			.hw_flow_control = 1, });
}

static int stb_7108_phy_reset(void *bus)
{
	static int done;

	/* This line is shared between both MII interfaces */
	if (!done) {
		gpio_set_value(STB_7108_PIO_POWER_ON_ETHERNET, 0);
		udelay(10000); /* 10 miliseconds is enough for everyone ;-) */
		gpio_set_value(STB_7108_PIO_POWER_ON_ETHERNET, 1);

		/**
		 * M.Schenk 2012.02.15
		 * KSZ8051 MNL need some time after reset
		 */
		udelay(10000);
		udelay(10000);
		udelay(10000);
		udelay(10000);

		done = 1;
	}

	return 1;
}

/**
 * M.Schenk 2012.05.21
 * KSZ8051NL IRQ goes to 7108 ETH1 MDINT on ILC EXT 25
 */
static struct stmmac_mdio_bus_data stmmac1_mdio_bus = {
	.bus_id = 1,
	.phy_reset = stb_7108_phy_reset,
	.phy_mask = 0,
/* M.Schenk 2012.05.21 get PHY IRQ to work */
#if 1
	.probed_phy_irq = ILC_IRQ(25),
#endif
};

/* NAND Flash CS is on CSN0 */
static struct stm_nand_bank_data stb_7108_nand_flash = {
	.csn		= 0,
	.options	= NAND_NO_AUTOINCR | NAND_USE_FLASH_BBT,
	.nr_partitions	= 4,
	.partitions	= (struct mtd_partition []) {
		{
			.name	= "NAND INITRAMFS Recovery",
			.offset	= 0,
			.size 	= 0x01800000					/* 0x00000000 ... 0x017fffff (24MiB) */
		}, {
			.name	= "NAND Kernel Main",
			.offset	= MTDPART_OFS_NXTBLK,
			.size	= 0x00400000					/* 0x01800000 ... 0x01bfffff (4MiB) */
		}, {
			.name	= "NAND Rootdisk Main",
			.offset	= MTDPART_OFS_NXTBLK,
			.size	= 0x05000000					/* 0x01c00000 ... 0x06bfffff (80MiB) */
		}, {
			.name	= "NAND Persistence",
			.offset	= MTDPART_OFS_NXTBLK,
			.size	= MTDPART_SIZ_FULL				/* 0x06c00000 ... 0x07ffffff (20MiB) */
		},
	},
	.timing_data = &(struct stm_nand_timing_data) {
		.sig_setup      = 10,           /* times in ns */
		.sig_hold       = 10,
		.CE_deassert    = 0,
		.WE_to_RBn      = 100,
		.wr_on          = 10,
		.wr_off         = 30,
		.rd_on          = 10,
		.rd_off         = 30,
		.chip_delay     = 30,           /* in us */
	},
};


/*
 * Serial Flash (Setup depends on the board revision and silicon cut.  See
 * comments at top).
 */
static struct mtd_partition stb_7108_serial_flash_parts[] = {
	{
		.name = "NOR U-BOOT",
		.size = 0x00080000,
		.offset = 0,
	}, {
		.name = "NOR U-BOOT ENV1",
		.size = 0x00010000,
		.offset = MTDPART_OFS_NXTBLK,
	}, {
		.name = "NOR U-BOOT ENV2",
		.size = 0x00010000,
		.offset = MTDPART_OFS_NXTBLK,
	}, {
		.name = "NOR Boot Screen",
		.size = 0x00060000,
		.offset = MTDPART_OFS_NXTBLK,
	}, {
		.name = "NOR Empty",
		.size = MTDPART_SIZ_FULL,
		.offset = MTDPART_OFS_NXTBLK,
	},
};

/* SPI_FSM: Serial Flash device */
static struct stm_plat_spifsm_data stb_7108_spifsm_flash = {
	.parts = stb_7108_serial_flash_parts,
	.nr_parts = ARRAY_SIZE(stb_7108_serial_flash_parts),
	.capabilities = {
		/* Capabilities may be overriden by SoC configuration */
		.dual_mode = 1,
		.quad_mode = 1,
	},
};

/* Albis related platform devices */
static struct platform_device stb_7108_kinjector_device = {
	.name		= "kinjector",
	.id		= -1,
	.num_resources	= 0,
};

static struct platform_device stb_7108_toolbox_device = {
	.name		= "toolbox",
	.id		= -1,
	.num_resources	= 0,
};

static struct platform_device stb_7108_ir_device = {
	.name		= "ir",
	.id		= -1,
	.num_resources	= 0,
};

static struct platform_device stb_7108_keypad_device = {
	.name		= "keypad",
	.id		= -1,
	.num_resources	= 0,
};

static struct platform_device stb_7108_vfd_device = {
	.name		= "vfd",
	.id		= -1,
	.num_resources	= 0,
};

static struct platform_device stb_7108_led_device = {
	.name = "led",
	.id = -1,
	.num_resources = 0,
};

static struct platform_device *stb_7108_devices[] __initdata = {
//	&stb_7108_nor_flash,
	/* Albis platform devices */
	&stb_7108_kinjector_device,
	&stb_7108_toolbox_device,
	&stb_7108_ir_device,
	&stb_7108_keypad_device,
	&stb_7108_vfd_device,
	&stb_7108_led_device,
};

/* M.Schenk 2012.05.08, don't whant peeks & pokes in stmwrapper, so do the SPDIF PAD config here */
static struct stm_pad_config stb_7108_spdif_player_pad_config = {
	.gpios_num = 1,
	.gpios = (struct stm_pad_gpio []) {
		STM_PAD_PIO_OUT(26, 0, 1),
	},
};

static struct stm_pad_state* stb_7108_spdif_player_pad_state = NULL;

static int __init device_init(void)
{
	struct sysconf_field *sc;
	unsigned long boot_mode;
	struct stm_plat_lpm_data board_info;

	/* Configure Flash according to boot-device */
	if (cpu_data->cut_major >= 2) {
		sc = sysconf_claim(SYS_STA_BANK1, 3, 2, 6, "boot_mode");
		boot_mode = sysconf_read(sc);
	} else {
		sc = sysconf_claim(SYS_STA_BANK1, 3, 2, 5, "boot_mode");
		boot_mode = sysconf_read(sc);
		boot_mode |= 0x10;	/* use non-BCH boot encodings */
	}
	switch (boot_mode) {
	case 0x15:
		pr_info("Configuring FLASH for boot-from-NOR\n");
		stb_7108_nand_flash.csn = 1;
		break;
	case 0x14:
		pr_info("Configuring FLASH for boot-from-NAND\n");
		stb_7108_nand_flash.csn = 0;
		break;
	case 0x1a:
		pr_info("Configuring FLASH for boot-from-SPI\n");
		stb_7108_nand_flash.csn = 0;
		break;
	default:
		BUG();
		break;
	}
	sysconf_release(sc);

	/* The "POWER_ON_ETH" line should be rather called "PHY_RESET",
	 * but it isn't... ;-) */
	gpio_request(STB_7108_PIO_POWER_ON_ETHERNET, "POWER_ON_ETHERNET");
	gpio_direction_output(STB_7108_PIO_POWER_ON_ETHERNET, 0);

/* M.Schenk 2012.02.15 TBD */
#if 0
	/* Some of the peripherals are powered by regulators
	 * triggered by the following PIO line... */
	gpio_request(STB_7108_PIO_POWER_ON, "POWER_ON");
	gpio_direction_output(STB_7108_PIO_POWER_ON, 1);
#endif

	/* update table for STM8 connected front panel*/
	board_info.number_i2c = 2;
	board_info.number_gpio = STB_7108_GPIO_LPM_I2C;

	/* NIM */
	stx7108_configure_ssc_i2c(1, NULL);
	/* AV */
	stx7108_configure_ssc_i2c(2, &(struct stx7108_ssc_config) {
			.routing.ssc2.sclk = stx7108_ssc2_sclk_pio14_4,
			.routing.ssc2.mtsr = stx7108_ssc2_mtsr_pio14_5, });
	/* SYS - EEPROM & MII0 */
	stx7108_configure_ssc_i2c(5, NULL);
	/* HDMI */
	stx7108_configure_ssc_i2c(6, NULL);

	/* Remark 2012.04.11, shall be used for our IR driver */
	stx7108_configure_lirc(&(struct stx7108_lirc_config) {
			.rx_mode = stx7108_lirc_rx_mode_ir, });

	stx7108_configure_usb(0);
	stx7108_configure_usb(1);
	stx7108_configure_usb(2);

	/* STB-9xxx members have only a single SATA */
#if 0
	/* 2 SATA's */
	stx7108_configure_miphy(&(struct stx7108_miphy_config) {
			.modes = (enum miphy_mode[2]) {
				SATA_MODE, SATA_MODE },
			});
	stx7108_configure_sata(0, &(struct stx7108_sata_config) { });
	stx7108_configure_sata(1, &(struct stx7108_sata_config) { });
#else
	stx7108_configure_miphy(&(struct stx7108_miphy_config) {
			.modes = (enum miphy_mode[2]) {
				SATA_MODE, UNUSED_MODE },
			});

	stx7108_configure_sata(0, &(struct stx7108_sata_config) { });
	stx7108_configure_lpm(&board_info);
#endif

/* M.Schenk 2012.05.21 get PHY IRQ to work */
#if 1
	set_irq_type(ILC_IRQ(25), IRQ_TYPE_LEVEL_LOW);
#endif

	stx7108_configure_ethernet(1, &(struct stx7108_ethernet_config) {
			.mode = stx7108_ethernet_mode_mii,
			.ext_clk = 0,
			.phy_bus = 1,
			.phy_addr = 1,
			.mdio_bus_data = &stmmac1_mdio_bus,
		});

	/*
	 * FLASH_WP is shared between between NOR and NAND FLASH.  However,
	 * since NAND MTD has no concept of write-protect, we permanently
	 * disable WP.
	 */
	gpio_request(STB_7108_GPIO_FLASH_WP, "FLASH_WP");
	gpio_direction_output(STB_7108_GPIO_FLASH_WP, 1);

	/* NAND Flash */
	stx7108_configure_nand(&(struct stm_nand_config) {
			.driver = stm_nand_flex,
			.nr_banks = 1,
			.banks = &stb_7108_nand_flash,
			.rbn.flex_connected = 1,});
	/*
	 * Serial Flash setup depends on board revision and silicon cut.  See
	 * comments above.
	 */
	stx7108_configure_spifsm(&stb_7108_spifsm_flash);

#ifdef CONFIG_SH_STB_7108_MMC_SLOT
	stx7108_configure_mmc(0);
#elif defined(CONFIG_SH_STB_7108_MMC_EMMC)
	stx7108_configure_mmc(1);
#endif

	stx7108_configure_audio(&(struct stx7108_audio_config) {
			.spdif_player_output_enabled = 1, });

	/**
	 * M.Schenk 2012.05.08
	 * configure SPDIF pad as alt output to get SPDIF signal on the pin.
	 */
	stb_7108_spdif_player_pad_state = stm_pad_claim(&stb_7108_spdif_player_pad_config, "SPDIF");

	return platform_add_devices(stb_7108_devices,
			ARRAY_SIZE(stb_7108_devices));
}
arch_initcall(device_init);


static void __iomem *stb_7108_ioport_map(unsigned long port, unsigned int size)
{
	/* If we have PCI then this should never be called because we
	 * are using the generic iomap implementation. If we don't
	 * have PCI then there are no IO mapped devices, so it still
	 * shouldn't be called. */
	BUG();
	return NULL;
}

struct sh_machine_vector mv_stb_7108 __initmv = {
	.mv_name = "stb_7108",
	.mv_setup = stb_7108_setup,
	.mv_nr_irqs = NR_IRQS,
	.mv_ioport_map = stb_7108_ioport_map,
	STM_PCI_IO_MACHINE_VEC
};

#ifdef CONFIG_HIBERNATION_ON_MEMORY

#include "../../kernel/cpu/sh4/stm_hom.h"

static int stb_7108_board_freeze(void)
{
/* M.Schenk 2012.02.15 TBD */
#if 0
	gpio_direction_output(STB_7108_PIO_POWER_ON, 0);
#endif
	return 0;
}

static int stb_7108_board_restore(void)
{
/* M.Schenk 2012.02.15 TBD */
#if 0
	/* Some of the peripherals are powered by regulators
	 * triggered by the following PIO line... */
	gpio_direction_output(STB_7108_PIO_POWER_ON, 1);
#endif

	/* The "POWER_ON_ETH" line should be rather called "PHY_RESET",
	 * but it isn't... ;-) */
	gpio_direction_output(STB_7108_PIO_POWER_ON_ETHERNET, 0);
	udelay(10000); /* 10 miliseconds is enough for everyone ;-) */
	gpio_direction_output(STB_7108_PIO_POWER_ON_ETHERNET, 1);

	/**
	 * M.Schenk 2012.02.15
	 * KSZ8051 MNL need some time after reset
	 */
	udelay(10000);
	udelay(10000);
	udelay(10000);
	udelay(10000);

	return 0;
}

static struct stm_hom_board stb_7108_hom = {
	.freeze = stb_7108_board_freeze,
	.restore = stb_7108_board_restore,
};

static int __init stb_7108_hom_register(void)
{
	return stm_hom_board_register(&stb_7108_hom);
}

module_init(stb_7108_hom_register);
#endif
