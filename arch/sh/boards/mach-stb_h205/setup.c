/*
 * arch/sh/boards/mach-stb_h205/setup.c
 *
 * Copyright (C) 2013 Albis Technologies Ldt.
 * Author: Michael Schenk (michael.schenk@albistechnologies.com)
 * Copyright (C) 2011 STMicroelectronics Limited
 * Author: Stuart Menefy (stuart.menefy@st.com)
 *
 * May be copied or modified under the terms of the GNU General Public
 * License.  See linux/COPYING for more information.
 */

/* M.Schenk : Based on arch/sh/boards/mach-b2064/setup.c */

#include <linux/init.h>
#include <linux/platform_device.h>
#include <linux/delay.h>
#include <linux/io.h>
#include <linux/phy.h>
#include <linux/gpio.h>
#include <linux/gpio_keys.h>
#include <linux/leds.h>
#include <linux/tm1668.h>
#include <linux/mtd/partitions.h>
#include <linux/mtd/nand.h>
#include <linux/spi/spi.h>
#include <linux/spi/flash.h>
#include <linux/stm/platform.h>
#include <linux/stm/stxh205.h>
#include <linux/stm/sysconf.h>
#include <asm/irq-ilc.h>

#define STB_H205_GPIO_FLASH_WP		stm_gpio(6, 2)
#define STB_H205_GPIO_POWER_ON_ETH	stm_gpio(3, 3)
#define STB_H205_POWER_ON          	stm_gpio(3, 7)

static void __init stb_h205_setup(char **cmdline_p)
{
#if defined(CONFIG_SH_STB_8083)
	printk(KERN_INFO "Albis Technologies Ldt STB-8083 board initialisation\n");
#elif defined(CONFIG_SH_STB_8090)
	printk(KERN_INFO "Albis Technologies Ldt STB-8090 board initialisation\n");
#else
	printk(KERN_INFO "Albis Technologies Ldt STB-H205 board initialisation\n");
#endif

	stxh205_early_device_init();

	/*
	 * Socket CN32 DB9-1 connector (no flow control)
	 */
	stxh205_configure_asc(STXH205_ASC(10), &(struct stxh205_asc_config) {
			.hw_flow_control = 0,
			.is_console = 1, });

//	/*
//	 * Socket CN33 DB9-2 connector (no flow control)
//	 * also CN29 as FSK UART (with flow conrtol)
//	 * also JS9 SMART1 (smartcard)
//	 * Note no jumpers to avoid problems with contention.
//	 */
//	stxh205_configure_asc(STXH205_ASC(1), &(struct stxh205_asc_config) {
//			.hw_flow_control = 0, });
}

//static struct platform_device b2064_leds = {
//	.name = "leds-gpio",
//	.id = -1,
//	.dev.platform_data = &(struct gpio_led_platform_data) {
//		.num_leds = 1,
//		.leds = (struct gpio_led[]) {
//			{
//				.name = "RED",
//				.default_trigger = "heartbeat",
//				.gpio = stm_gpio(3, 1),
//			},
//			/*
//			 * Its not clear what's happening here, but it
//			 * appears as if STxH239 has three balls connected
//			 * on the b2064 as:
//			 *   PIO32 - 7108_LMI_RET#
//			 *   PIO33 - LED_GREEN
//			 *   PIO32B - LED_GREEN
//			 * The net effects appers to be a short between
//			 * LMI_RET and LED GREEN which means any attemmpt
//			 * to use the LED causes the system to crash.
//			 *
//			 * This has been fixed on B2064 revB, so if you know
//			 * you're on a rev B uncomment the following block
//			 * and change .num_leds above to 2.
//			 * {
//			 *	.name = "GREEN",
//			 *	.gpio = stm_gpio(3, 3),
//			 * },
//			 */
//		},
//	},
//};

//static struct tm1668_key b2064_front_panel_keys[] = {
//	{ 0x00001000, KEY_UP, "Up (SWF2)" },
//	{ 0x00800000, KEY_DOWN, "Down (SWF7)" },
//	{ 0x00008000, KEY_LEFT, "Left (SWF6)" },
//	{ 0x00000010, KEY_RIGHT, "Right (SWF5)" },
//	{ 0x00000080, KEY_OK, "Menu/OK (SWF1)" },
//	{ 0x00100000, KEY_BACK, "Back (SWF4)" },
//	{ 0x80000000, KEY_TV, "DOXTV (SWF9)" },
//};
//
//static struct tm1668_character b2064_front_panel_characters[] = {
//	TM1668_7_SEG_HEX_DIGITS,
//	TM1668_7_SEG_SEGMENTS,
//	TM1668_7_SEG_LETTERS
//};
//
//static struct platform_device b2064_front_panel = {
//	.name = "tm1668",
//	.id = -1,
//	.dev.platform_data = &(struct tm1668_platform_data) {
//		.gpio_dio = stm_gpio(15, 4),
//		.gpio_sclk = stm_gpio(14, 7),
//		.gpio_stb = stm_gpio(14, 4),
//		.config = tm1668_config_6_digits_12_segments,
//
//		.keys_num = ARRAY_SIZE(b2064_front_panel_keys),
//		.keys = b2064_front_panel_keys,
//		.keys_poll_period = DIV_ROUND_UP(HZ, 5),
//
//		.brightness = 8,
//		.characters_num = ARRAY_SIZE(b2064_front_panel_characters),
//		.characters = b2064_front_panel_characters,
//		.text = "H239",
//	},
//};

//static struct gpio_keys_button b2064_fp_gpio_keys_button = {
//	.code = KEY_SUSPEND,
//	.gpio = stm_gpio(15, 7),
//	.desc = "Standby",
//};
//
//static struct platform_device b2064_fp_gpio_keys = {
//        .name = "gpio-keys",
//        .id = -1,
//        .num_resources = 0,
//        .dev = {
//                .platform_data = &(struct gpio_keys_platform_data){
//			.buttons = &b2064_fp_gpio_keys_button,
//			.nbuttons = 1,
//		}
//        }
//};

/* Serial Flash */
static struct stm_plat_spifsm_data stb_h205_serial_flash =  {
/* M.Schenk 2013.09.12 no name to get ride of warnings */
#if 0
	.name		= "MX25L1655D",
#else
	.name		= NULL,
#endif
	.nr_parts	= 5,
	.parts = (struct mtd_partition []) {
		{
			.name = "NOR U-BOOT",
			.size = 0x00080000,
			.offset = 0,
		}, {
			.name = "NOR U-BOOT ENV1",
			.size = 0x00010000,
			.offset = MTDPART_OFS_NXTBLK,
		}, {
			.name = "NOR U-BOOT ENV2",
			.size = 0x00010000,
			.offset = MTDPART_OFS_NXTBLK,
		}, {
			.name = "NOR Boot Screen",
			.size = 0x00060000,
			.offset = MTDPART_OFS_NXTBLK,
		}, {
			.name = "NOR Empty",
			.size = MTDPART_SIZ_FULL,
			.offset = MTDPART_OFS_NXTBLK,
		},
	},
	.capabilities = {
		/* Capabilities may be overriden by SoC configuration */
		.dual_mode = 1,
		.quad_mode = 1,
		.reset_signal = 0, /* Reset signal can be routed to UD16 if RD21
				    * fitted (default is DNF)
				    */
	},
};

/* NAND Flash */
static struct stm_nand_bank_data stb_h205_nand_flash = {
	.csn		= 0,	/* Rev A/B : set SW4 2-3 (EMI_CS0 -> NAND_CS)
				 * Rev C   : EMI_CS0 hardwired to NAND_CS
				 */
	.options	= NAND_NO_AUTOINCR | NAND_USE_FLASH_BBT,
	.nr_partitions	= 4,
	.partitions	= (struct mtd_partition []) {
		{
			.name	= "NAND INITRAMFS Recovery",
			.offset	= 0,
			.size 	= 0x01800000					/* 0x00000000 ... 0x017fffff (24MiB) */
		}, {
			.name	= "NAND Kernel Main",
			.offset	= MTDPART_OFS_NXTBLK,
			.size	= 0x00400000					/* 0x01800000 ... 0x01bfffff (4MiB) */
		}, {
			.name	= "NAND Rootdisk Main",
			.offset	= MTDPART_OFS_NXTBLK,
			.size	= 0x05000000					/* 0x01c00000 ... 0x06bfffff (80MiB) */
		}, {
			.name	= "NAND Persistence",
			.offset	= MTDPART_OFS_NXTBLK,
			.size	= MTDPART_SIZ_FULL				/* 0x06c00000 ... 0x07ffffff (20MiB) */
		},
	},
	.timing_data	=  &(struct stm_nand_timing_data) {
		.sig_setup	= 50,		/* times in ns */
		.sig_hold	= 50,
		.CE_deassert	= 0,
		.WE_to_RBn	= 100,
		.wr_on		= 10,
		.wr_off		= 40,
		.rd_on		= 10,
		.rd_off		= 40,
		.chip_delay	= 30,		/* in us */
	},
};

static int stb_h205_phy_reset(void *bus)
{
	/*
	 * IC+ IP101 datasheet specifies 10mS low period and device usable
	 * 2.5mS after rising edge. However experimentally it appear
	 * 10mS is required for reliable functioning.
	 */
	gpio_set_value(STB_H205_GPIO_POWER_ON_ETH, 0);
	mdelay(10);
	gpio_set_value(STB_H205_GPIO_POWER_ON_ETH, 1);
	mdelay(10);

	return 1;
}

static struct stmmac_mdio_bus_data stmmac_mdio_bus = {
	.bus_id = 0,
	.phy_reset = stb_h205_phy_reset,
	.phy_mask = 0,
	.probed_phy_irq = ILC_IRQ(25), /* MDINT */
};


/* Albis related platform devices */
static struct platform_device stb_h205_kinjector_device = {
	.name		= "kinjector",
	.id		= -1,
	.num_resources	= 0,
};

static struct platform_device stb_h205_toolbox_device = {
	.name		= "toolbox",
	.id		= -1,
	.num_resources	= 0,
};

static struct platform_device stb_h205_ir_device = {
	.name		= "ir",
	.id		= -1,
	.num_resources	= 0,
};

/* maybe we will use the recovery boot button later */
static struct platform_device stb_h205_keypad_device = {
	.name		= "keypad",
	.id		= -1,
	.num_resources	= 0,
};

static struct platform_device stb_h205_led_device = {
	.name = "led",
	.id = -1,
	.num_resources = 0,
};

//static struct platform_device *b2064_devices[] __initdata = {
//	&b2064_leds,
//	&b2064_front_panel,
//	&b2064_fp_gpio_keys,
//};

static struct platform_device *stb_h205_devices[] __initdata = {
	/* Albis platform devices */
	&stb_h205_kinjector_device,
	&stb_h205_toolbox_device,
	&stb_h205_ir_device,
	&stb_h205_keypad_device,
	&stb_h205_led_device,
};

/* the whole stxh205_audio.c stuff is missing see Bugzilla #31728 */
#if 0
/* M.Schenk 2012.05.08, don't whant peeks & pokes in stmwrapper, so do the SPDIF PAD config here */
static struct stm_pad_config stb_h205_spdif_player_pad_config = {
	.gpios_num = 1,
	.gpios = (struct stm_pad_gpio []) {
		STM_PAD_PIO_OUT(4, 1, 1),
	},
};

static struct stm_pad_state* stb_h205_spdif_player_pad_state = NULL;
#endif

static int __init device_init(void)
{
	/* The "POWER_ON_ETH" line should be rather called "PHY_RESET",
	 * but it isn't... ;-) */
	gpio_request(STB_H205_GPIO_POWER_ON_ETH, "POWER_ON_ETH");
	gpio_direction_output(STB_H205_GPIO_POWER_ON_ETH, 0);

	gpio_request(STB_H205_POWER_ON, "POWER_ON");
	gpio_direction_output(STB_H205_POWER_ON, 1);

	stxh205_configure_ethernet(&(struct stxh205_ethernet_config) {
			.mode = stxh205_ethernet_mode_mii,
			.ext_clk = 1,
			.phy_bus = 0,
			.phy_addr = -1,
			.mdio_bus_data = &stmmac_mdio_bus,
		});

	/* PHY IRQ has to be triggered LOW */
	set_irq_type(ILC_IRQ(25), IRQ_TYPE_LEVEL_LOW);

/* M.Schenk, no SATA */
#if 0
	stxh205_configure_miphy(&(struct stxh205_miphy_config){
			.mode = SATA_MODE,
			.iface = UPORT_IF,
		});
	stxh205_configure_sata();
#endif

	/**
	 * M.Schenk
	 * STB-8083 has only USB1 as front USB
	 */
#if defined(CONFIG_SH_STB_8090)
	stxh205_configure_usb(0);
#endif
	stxh205_configure_usb(1);

/* M.Schenk, we only have I2C for HDMI */
#if 0
	/* SSC1: FE - U14: LNBH26PQR, STxH239: J_SCL/SDA (internal demod), JB6 */
	stxh205_configure_ssc_i2c(STXH205_SSC(1), &(struct stxh205_ssc_config) {
			.routing.ssc1.sclk = stxh205_ssc1_sclk_pio12_0,
			.routing.ssc1.mtsr = stxh205_ssc1_mtsr_pio12_1, });

	/* SSC3: SYS - STV6440, EEPROM, Front panel */
	stxh205_configure_ssc_i2c(STXH205_SSC(3), &(struct stxh205_ssc_config) {
			.routing.ssc3.sclk = stxh205_ssc3_sclk_pio15_5,
			.routing.ssc3.mtsr = stxh205_ssc3_mtsr_pio15_6, });
#endif
	/* SSC11: HDMI */
	stxh205_configure_ssc_i2c(STXH205_SSC(11), NULL);

#if defined(CONFIG_SH_STB_8090)
	/* SSC3: STV6430 video switch on STB-8090 */
	stxh205_configure_ssc_i2c(STXH205_SSC(3), &(struct stxh205_ssc_config) {
			.routing.ssc3.sclk = stxh205_ssc3_sclk_pio15_5,
			.routing.ssc3.mtsr = stxh205_ssc3_mtsr_pio15_6, });
#endif

/* M.Schenk, we only have I2C for HDMI */
#if 0
	/* SSC2: NIM: CN29 */
	stxh205_configure_ssc_i2c(STXH205_SSC(2), &(struct stxh205_ssc_config) {
			.routing.ssc2.sclk = stxh205_ssc2_sclk_pio9_4,
			.routing.ssc2.mtsr = stxh205_ssc2_mtsr_pio9_5, });
#endif

	stxh205_configure_lirc(&(struct stxh205_lirc_config) {
			.rx_mode = stxh205_lirc_rx_mode_ir, });

/* M.Schenk, no PWM */
#if 0
	stxh205_configure_pwm(&(struct stxh205_pwm_config) {
			/*
			 * PWM10 is connected to 12V->1.2V power supply
			 * for "debug purposes". Enable at your own risk!
			 */
			.out10_enabled = 0 });
#endif

/* M.Schenk 2013.12.19, STB-8090 has a MMC */
#if defined(CONFIG_SH_STB_8090)
	stxh205_configure_mmc(&(struct stxh205_mmc_config) {
			.emmc = 0,
			.no_mmc_boot_data_error = 1,
		});
#endif

	/*
	 * NAND MTD has no concept of write-protect, so permanently disable WP
	 */
	gpio_request(STB_H205_GPIO_FLASH_WP, "FLASH_WP");
	gpio_direction_output(STB_H205_GPIO_FLASH_WP, 1);

/**
 * M.Schenk 2013.07.04
 * 4 Bit ECC BCH controller will not work with JFFS2
 */
#if 0
	stxh205_configure_nand(&(struct stm_nand_config) {
			.driver = stm_nand_bch,
			.nr_banks = 1,
			.banks = &stb_h205_nand_flash,
			.rbn.flex_connected = 1,
			.bch_ecc_cfg = BCH_ECC_CFG_AUTO});
#else
	stxh205_configure_nand(&(struct stm_nand_config) {
			.driver = stm_nand_flex,
			.nr_banks = 1,
			.banks = &stb_h205_nand_flash,
			.rbn.flex_connected = 1,});
#endif

	stxh205_configure_spifsm(&stb_h205_serial_flash);

/* the whole stxh205_audio.c stuff is missing see Bugzilla #31728 */
#if 0
	stxh205_configure_audio(&(struct stxh205_audio_config) {
			.spdif_player_output_enabled = 1, });

	/**
	 * M.Schenk 2013.06.07
	 * configure SPDIF pad as alt output to get SPDIF signal on the pin.
	 */
	stb_h205_spdif_player_pad_state = stm_pad_claim(&stb_h205_spdif_player_pad_config, "SPDIF");
#endif

	return platform_add_devices(stb_h205_devices,
			ARRAY_SIZE(stb_h205_devices));
}
arch_initcall(device_init);

static void __iomem *stb_h205_ioport_map(unsigned long port, unsigned int size)
{
	/* If we have PCI then this should never be called because we
	 * are using the generic iomap implementation. If we don't
	 * have PCI then there are no IO mapped devices, so it still
	 * shouldn't be called. */
	BUG();
	return NULL;
}

struct sh_machine_vector mv_stb_h205 __initmv = {
	.mv_name = "stb_h205",
	.mv_setup = stb_h205_setup,
	.mv_nr_irqs = NR_IRQS,
	.mv_ioport_map = stb_h205_ioport_map,
};

#if defined(CONFIG_HIBERNATION_ON_MEMORY)

#include "../../kernel/cpu/sh4/stm_hom.h"

static int stb_h205_board_freeze(void)
{
	gpio_set_value(STB_H205_GPIO_POWER_ON_ETH, 0);
	return 0;
}

static int stb_h205_board_defrost(void)
{
	stb_h205_phy_reset(NULL);
	return 0;
}

static struct stm_hom_board stb_h205_hom = {
	.freeze = stb_h205_board_freeze,
	.restore = stb_h205_board_defrost,
};

static int __init stb_h205_hom_register(void)
{
	return stm_hom_board_register(&stb_h205_hom);
}

module_init(stb_h205_hom_register);
#endif

