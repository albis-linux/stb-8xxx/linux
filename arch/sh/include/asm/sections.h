#ifndef __ASM_SH_SECTIONS_H
#define __ASM_SH_SECTIONS_H

#include <asm-generic/sections.h>

extern long __nosave_begin, __nosave_end;
extern long __machvec_start, __machvec_end;

/**
 * M.Schenk 2013.07.09
 * optimized WA for large INITRAMFS on STB-8xxx platform which
 * has no impact to other platforms.
 */
#if defined(CONFIG_SH_STB_8000) || defined(CONFIG_SH_STB_8080) || defined(CONFIG_SH_STB_8100) || defined(CONFIG_SH_STB_8500)
extern char __uncached_start, __uncached_end;
#else
extern char __uncached_start[], __uncached_end[];
#endif

extern char _ebss[];
extern char __start_eh_frame[], __stop_eh_frame[];

#endif /* __ASM_SH_SECTIONS_H */

