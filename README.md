# Albis STB-8xxx Linux

Taken from [8000.ZAP.01.XX.0641.37](http://opensource.albistechnologies.com/STB/STB-8000/8000.ZAP.01.XX.0641.37.htm)

### Usage

This kernel needs gcc 4.x.  
[This STLinux toolchain](https://gitlab.com/albis-linux/toolchain) is tested and produces valid binaries.

Steps:
- `export ARCH=sh`
- `export CROSS_COMPILE=/path/to/toolchain/sh4-linux-`
- `make hdk7105_defconfig`
- change Physical memory size to 0x10000000
- change Board support to STB-8000
- `make uImage`
