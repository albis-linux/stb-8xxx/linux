/*---------------------------------------------------------------------------
* /include/linux/stm/lpm.h
* Copyright (C) 2011 STMicroelectronics Limited
* Contributor:Francesco Virlinzi <francesco.virlinzi@st.com>
* Author:Pooja Agarwal <pooja.agarwal@st.com>
* Author:Udit Kumar <udit-dlh.kumar@st.cm>
* May be copied or modified under the terms of the GNU General Public
* License.  See linux/COPYING for more information.
*----------------------------------------------------------------------------
*/
#ifndef __LPM_H
#define __LPM_H

#include <linux/rtc.h>

/*
* stm_lpm_wakeup_devices
*
* Define wakeup devices
* One bit for each wakeup device
*/

enum stm_lpm_wakeup_devices{
	STM_LPM_WAKEUP_IR = 1<<0,
	STM_LPM_WAKEUP_CEC = 1<<1,
	STM_LPM_WAKEUP_FRP = 1<<2,
	STM_LPM_WAKEUP_WOL = 1<<3,
	STM_LPM_WAKEUP_RTC = 1<<4,
	STM_LPM_WAKEUP_ASC = 1<<5,
	STM_LPM_WAKEUP_NMI = 1<<6,
	STM_LPM_WAKEUP_HPD = 1<<7,
	STM_LPM_WAKEUP_PIO = 1<<8,
	STM_LPM_WAKEUP_EXT = 1<<9
};

/*
* stm_lpm_reset_type
*
* Define reset type
* SOC reset or SBC reset
* Boot reset is SBC will reset and stay in bootloader
*/
enum stm_lpm_reset_type{
	STM_LPM_SOC_RESET = 0,
	STM_LPM_SBC_RESET = 1<<0,
	STM_LPM_BOOT_RESET = 1<<1
};

/*
* stm_lpm_sbc_state
*
* Define state of SBC software
* STM_LPM_SBC_BOOT - SBC waiting in bootloader
* STM_LPM_SBC_WAIT - SBC waiting for host to ready
* STM_LPM_SBC_RUNNING - SBC is running
* STM_LPM_SBC_STANDBY - Entering into standby
*/

enum stm_lpm_sbc_state{
	STM_LPM_SBC_BOOT = 1,
	STM_LPM_SBC_WAIT = 3,
	STM_LPM_SBC_RUNNING = 4,
	STM_LPM_SBC_STANDBY = 5
};

/*
* stm_lpm_version
*
* Defines the version information of stlpm driver and SBC firmware
* This has three fields
* Protocol version (Major, Minor)
* version (major, minor and patch) and
* build information (day, month and year)
*/
struct stm_lpm_version{
	char major_comm_protocol;
	char minor_comm_protocol;
	char major_soft;
	char minor_soft;
	char patch_soft;
	char month;
	char day;
	char year;
};

/*
* stm_lpm_fp_setting
*
* Defines the frontpanel display settings
* owner when 0 - SBC firmware will owner in standby
* owner when 1 - SBC firmwware always owner frontpanel display
* owner when 2 - Host will always own front panel
* am_pm when 0 clock will be displayed in 24 hrs format
*/
struct stm_lpm_fp_setting{
	char owner;
	char am_pm;
	char brightness;
};

/*
* stm_lpm_pio_level
*
* Define level of PIO High ot LOW
* STM_LPM_PIO_LOW indicate power off/IT will be generated when goes low
* STM_LPM_PIO_HIGH indicate power off/IT will be generated when goes high
*/

enum stm_lpm_pio_level{
	STM_LPM_PIO_LOW = 0,
	STM_LPM_PIO_HIGH = 1 << 7
};

/*
* stm_lpm_pio_direction
*
* Define direction of PIO input or output
*/
enum stm_lpm_pio_direction{
	STM_LPM_PIO_INPUT = 0,
	STM_LPM_PIO_OUTPUT = 1<<5
};

/*
* stm_lpm_pio_use
*
* Define use of PIO - power control, MDINT, wakeup(normal gpio for wakeup)
*/

enum stm_lpm_pio_use{
	STM_LPM_PIO_POWER = 1,
	STM_LPM_PIO_ETH_MDINT = 2,
	STM_LPM_PIO_WAKEUP = 3,
	STM_LPM_PIO_EXT_IT = 4,
	STM_LPM_PIO_OTHER = 5,
	STM_LPM_PIO_FP_PIO = 6,
};

/*
* stm_lpm_pio_setting
*
* Defines pio configutaion
*/

struct stm_lpm_pio_setting{
	enum stm_lpm_pio_level pio_level;
	char interrupt_enabled; /* This is valid if PIO is used as interrupt*/
	enum stm_lpm_pio_direction pio_direction;
	enum stm_lpm_pio_use  pio_use;
	char pio_bank;
	char pio_pin;
};

/*
* stm_lpm_adv_feature_name
* Define advance features of SBC firmware
* external Vcore, Internal voltage detect, external clocking, RTC source
* and wakeup trigerrs
*/

enum stm_lpm_adv_feature_name{
	STM_LPM_USE_EXT_VCORE = 1,
	STM_LPM_USE_INT_VOLT_DETECT = 2,
	STM_LPM_EXT_CLOCK = 3,
	STM_LPM_RTC_SOURCE = 4,
	STM_LPM_WU_TRIGGERS = 5,
};

/*
* stm_lpm_adv_feature
* Define SBC advance feature
* feature_name is selected feature
* feature_parameter is parameter for selected feature
*
* when features is STM_LPM_USE_EXT_VCORE, parameter 0 means Internal Vcore
* parmeter 1 means extenal Vcore
*
* when feature is STM_LPM_USE_INT_VOLT_DETECT , in Parameter[0] specify voltage
* to detect use voltage*10 i.e for 3.3V use 33 and for 3.0V use 30
*
* When feature is STM_LPM_EXT_CLOCK  in parameter[0] use 1 for EXTERNAL,
* use 2 for AGC_EXTERNAL and 3 for Track_32K
*
* When feature is STM_LPM_RTC_SOURCE in parameter[0] use 1 for RTC_32K_TCXO
* and  2 for RTC_32K_OSC
*
* When feature is STM_LPM_WU_TRIGGERS in parameter[0-1] use enabled wakeup
* triggger's bit map  as specified in stm_lpm_wakeup_devices
*/

struct stm_lpm_adv_feature{
	enum stm_lpm_adv_feature_name feature_name;
	unsigned char feature_parameter[2];
};

/* defines  MAX depth for IR FIFO */
#define MAX_IR_FIFO_DEPTH 64

/*
* stm_lpm_ir_fifo
* Define data associated with IR IP mark and symbol
*/
struct stm_lpm_ir_fifo{
	u16 mark;
	u16 symbol;
};

/*
* stm_lpm_ir_key
* Define data associated with IR key
* key number (key_index)
* mark and symbol array to represent that key
* num of mark/symol patterns, Max value is 64
*/

struct stm_lpm_ir_key{
	u8 key_index;
	u8 num_patterns;
	struct  stm_lpm_ir_fifo fifo[MAX_IR_FIFO_DEPTH];
};


/*
* stm_lpm_ir_keyinfo
* Define Ir key and IR protocol specific data
* ir_id when device to use 0 for first IR device and 0x80 for first UHF device
* time_period and time_out protocol specific data
* tolerance variance from default values and , Default is 10%
* ir_key key information
*/
struct stm_lpm_ir_keyinfo{
	u8 ir_id;
	u16 time_period;
	u16 time_out;
	u8 tolerance;
	struct stm_lpm_ir_key ir_key;
};

/*
 * stm_lpm_cec_address
 *  Define CEC address 
 *  Physical address array in format 
 * phy_addr[0].phy_addr[1].phy_addr[2].phy_addr[3]
 *  represent a.b.c.d 
 * logical_addr is bit field, eachh bit set 
 * enables its logical address  
 */
struct stm_lpm_cec_address
{
	u16 phy_addr;
	u16 logical_addr;	 
}; 

/*
 * stm_lpm_cec_select
 * to define CEC parameter type
 * STM_LPM_CONFIG_CEC_WU_REASON Enabled disable CEC WU reason
 * STM_LPM_CONFIG_CEC_WU_CUSTOM_MSG Set Custom message for CEC WU 
 *
 */
enum stm_lpm_cec_select
{
	STM_LPM_CONFIG_CEC_WU_REASON=1,
	STM_LPM_CONFIG_CEC_WU_CUSTOM_MSG=2,
};

/*
 * stm_lpm_cec_wu_reason
 * Define CEC WU reason
 * STM_LPM_CEC_WU_STREAM_PATH opcode 0x86
 * STM_LPM_CEC_WU_USER_POWER opcode 0x44, oprand 0x40
 * STM_LPM_CEC_WU_STREAM_POWER_ON opcode 0x44, oprand 0x6B
 * STM_LPM_CEC_WU_STREAM_POWER_TOGGLE opcode 0x44, oprand 0x6D
 * STM_LPM_CEC_WU_USER_MSG user defined message
*/

enum stm_lpm_cec_wu_reason
{
	STM_LPM_CEC_WU_STREAM_PATH=1,
	STM_LPM_CEC_WU_USER_POWER=2,
	STM_LPM_CEC_WU_STREAM_POWER_ON=4,
	STM_LPM_CEC_WU_STREAM_POWER_TOGGLE=8,
	STM_LPM_CEC_WU_USER_MSG=16,
	
};
/*
 * stm_lpm_cec_custom_msg 
 * Define CEC custom message
 * size of CEC message including oprand and opcode
 * opcode - 
 * oprand  - 
*/
struct stm_lpm_cec_custom_msg {
	u8 size;
	u8 opcode;
	u8 oprand[10];	
};

/*
 * stm_lpm_cec_params
 * to define CEC params 
 * cec_wu_reasons - Bit field for each CEC wakeup device
 * Bits should be set as per enum stm_lpm_cec_wu_reason, 
 * to enable a CEC WU reason 
 * cec_msg - user defined CEC message 	
 */
union stm_lpm_cec_params 
{
	u8 cec_wu_reasons;
	struct stm_lpm_cec_custom_msg cec_msg;	 
};
int stm_lpm_configure_wdt(u16 time_in_ms);

int stm_lpm_get_fw_state(enum stm_lpm_sbc_state *fw_state);

int stm_lpm_get_wakeup_device(enum stm_lpm_wakeup_devices *wakeupdevice);

int stm_lpm_get_wakeup_info(enum stm_lpm_wakeup_devices *wakeupdevice,
	int *validsize, int datasize, char  *data) ;

int stm_lpm_get_version(struct stm_lpm_version *drv_ver,
	struct stm_lpm_version *fw_ver);

int stm_lpm_reset(enum stm_lpm_reset_type reset_type);

int stm_lpm_setup_fp(struct stm_lpm_fp_setting *fp_setting);


int stm_lpm_setup_ir(u8 num_keys, struct stm_lpm_ir_keyinfo *keys);

int stm_lpm_set_rtc(struct rtc_time *new_rtc);

int stm_lpm_set_wakeup_device(u16  wakeup_devices);

int stm_lpm_set_wakeup_time(u32 timeout);

int stm_lpm_setup_pio(struct stm_lpm_pio_setting *pio_setting);

int stm_lpm_setup_keyscan(u16 key_data);

int stm_lpm_set_adv_feature(u8 enabled, struct stm_lpm_adv_feature *feature);

int stm_lpm_get_adv_feature(unsigned char all_features, char *features);

/* This API to update new SBC firmware at run-time */
/* usage update new firmware in lib/firmware area */
/* After updating firmware in above area call this API */
int stm_lpm_reload_sbc_firmware(void); 

/* 
 *	return of this callback function 
 *	should be 0 if immediate reset is required <i.e time out value is zero>
 *	should be any positive value i.e timeout in ms after which reset is required 
 *	should be any negative value if not needed to send a ack to SBC for this long key press. 
 */
typedef int (*stm_lpm_reset_notifier_fn) (void);

/* to register user reset function */
void stm_lpm_register_reset_callback(stm_lpm_reset_notifier_fn user_fn); 

/* 
 * function to inform SBC about FP PIO
 * This PIO is used to detect FP PIO Long press as defined in long_press_delay in ms 
 * After detecting GP PIO long press SBC will send message to Host to invoke call back stm_lpm_reset_notifier_fn
 * if no reply from host then SBC will reset the SOC after delay specified in default_reset_delay ms
 * if stm_lpm_reset_notifier_fn specify some other time then SBC will reset SOC after that delay  ms
 */
int stm_lpm_setup_fp_pio(struct stm_lpm_pio_setting *pio_setting, u32 long_press_delay, u32 default_reset_delay); 

int stm_lpm_setup_power_on_delay(u16 de_bounce_delay, u16 dc_stability_delay);

int stm_lpm_set_cec_addr(struct stm_lpm_cec_address *addr); 

int stm_lpm_cec_config(enum stm_lpm_cec_select use, union stm_lpm_cec_params *params);
 
#endif /*__LPM_H*/

